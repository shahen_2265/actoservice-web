import axios from 'axios';

const HOST = 'http://0.0.0.0:3000';
const API = `${HOST}/api/v1`;
const SIGNUP_API = `${API}/auth/signup`;

const payload = {
  name: 'Shahen',
  phone: '+37455095466',
  email: 'dummmy@mail.ru',
  password: 'super puper dummy password'
};

function createUser() {
  return axios.post(SIGNUP_API, payload);
}

function removeUser(token) {
  return axios.get(`${API}/users`, {
    headers: {
      Authorization: token,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }
  });
}

global.HOST = HOST;
global.API = API;
global.USER_CREDENTIALS = payload;
global.removeUser = removeUser;
global.createUser = createUser;
