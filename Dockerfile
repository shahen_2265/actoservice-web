FROM node:9.4.0

# HEALTHCHECK --interval=10s --timeout=30s --start-period=5s --retries=3 CMD ["curl", "-fs", "http://localhost:${PORT:-8000}"]
ARG workdir=/var/app/actoservice

RUN mkdir -p ${workdir}

ADD . ${workdir}

WORKDIR ${workdir}

RUN npm i -g nodemon
RUN npm i

EXPOSE 3000
EXPOSE 9000

CMD ["npm", "start"]
