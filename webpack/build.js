const webpack = require('webpack');

const config = require('../webpack.config.actoservice');

webpack(config, (err, stats) => {
  if (err) throw err;
  if (stats.hasErrors()) {
    throw new Error(stats.toString());
  }
});
