const isActoserviceBuild = !!process.env.ACTOSERVICE_BUILD;

const WebpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');
const chalk = require('chalk');

const config = require('../webpack.config.actoservice');

console.log(chalk.blue.underline.bold(`\n\n\n\nRunning webpack to build ${config.name}\n\n\n\n`));

const compiler = webpack(config);

const serverConfig = {
  hot: true,
  inline: true,
  host: '0.0.0.0',
  disableHostCheck: true,
  historyApiFallback: true,
  contentBase: config.devServer.contentBase,
  headers: { 'Access-Control-Allow-Origin': '*' },
};

const server = new WebpackDevServer(compiler, serverConfig);

server.listen(3001);
