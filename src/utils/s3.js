/**
 * @flow
 */

import AWS from 'aws-sdk';
import { createReadStream } from 'fs';
import isEmpty from 'lodash/isEmpty';
import { s3Util } from 'utils/resources';
import mime from 'mime-types';
import https from 'https';
import config from 'config';

type ThemeUploadOptions = {
  key: string,
  source: {
    file: string,
  },
  configMap: {
    file: string
  },
  image?: {
    file: string
  },
  assets: {
    file: string,
    key: string
  }[]
}

type RawUpload = {
  key: string,
  contentType?: string,
  file: string,
  rawString?: boolean
};

type FetchS3Options = {
  disablePrefix?: boolean
};

type TemplateUploadOptions = {
  key: string,
  scheme: string,
};

type UploadResourceOptions = {
  file: string,
  key: string,
  contentType: string,
  apiKey: string
};

type UploadTemplateResourceOptions = {
  file: string,
  key: string,
  contentType: string,
  organizationId: string
};

type S3InterfaceResponse = Promise<{
  err: ?any,
  data: ?{}
}>;

const { S3_BUCKET } = process.env;

if (!S3_BUCKET) {
  throw new Error('S3_BUCKET deosnt set');
}

const STORAGE_API = config.s3.host;

class S3 {
  s3: AWS.S3;

  constructor() {
    AWS.config.update({
      accessKeyId: process.env.AWS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_KEY,
      region: process.env.AWS_REGION
    });

    this.s3 = new AWS.S3();
  }

  locationToCloudfront = (location: string) => {
    const splitFrom = location.indexOf(S3_BUCKET) + S3_BUCKET.length;

    return `${STORAGE_API}${location.substr(splitFrom)}`;
  };

  getObject = (name: string, options?: FetchS3Options): S3InterfaceResponse =>
    this._getFileFromS3(name, options);

  _getFileFromS3 = (path: string, options: FetchS3Options = {}): S3InterfaceResponse => {
    const fullPath = options.disablePrefix
      ? path
      : encodeURI(`${STORAGE_API}/${path}`);

    return new Promise((resolve) => {
      https.get(fullPath, (response) => {
        const chunks = [];
        response.on('error', () => resolve({ err: 'Cant read response', data: null }));
        response.on('data', chunk => chunks.push(chunk.toString()));
        response.on('end', () => {
          try {
            const json = JSON.parse(chunks.join(''));
            resolve({ data: json, err: null });
          } catch (e) {
            // eslint-disable-next-line
            resolve({ err: e, data: null });
          }
        });
      });
    });
  };

  uploadTemplate = ({
    key,
    scheme
  }: TemplateUploadOptions) => this._upload({
    key: `templates/o/${key}/template.json`,
    file: scheme,
    rawString: true,
    contentType: 'application/json'
  });

  uploadTheme({
    key,
    source,
    configMap,
    image,
    assets
  }: ThemeUploadOptions) {
    const promises = [];

    const uploadSource = this._upload({
      key: `${key}/bundle.js`,
      contentType: 'text/javascript',
      file: source.file
    });
    const uploadScheme = this._upload({
      key: `${key}/scheme.json`,
      contentType: 'application/json',
      file: configMap.file
    });

    promises.push(uploadSource);
    promises.push(uploadScheme);

    if (!isEmpty(image)) {
      const imgName = `${Date.now() + (Math.random() * 1000)}.${mime.extension(mime.lookup(image.file))}`;

      promises.push(this._upload({
        key: `${key}/${imgName}`,
        file: image.file,
        contentType: mime.lookup(image.file)
      }));
    }

    if (!isEmpty(assets)) {
      assets.forEach((asset) => {
        const uploadPromise = this._upload({
          key: `${key}/${asset.key}`,
          file: asset.file,
          contentType: mime.lookup(asset.file)
        });
        promises.push(uploadPromise);
      });
    }
    return Promise.all(promises);
  }

  uploadResource = ({
    file,
    key,
    contentType,
    apiKey
  }: UploadResourceOptions) =>
    this._upload({
      key: `${s3Util.resourcesPath(apiKey).folder}/${key}`,
      file,
      contentType
    });

  uploadTemplateResource = ({
    file,
    key,
    contentType,
    organizationId
  }: UploadTemplateResourceOptions) =>
    this._upload({
      key: `${s3Util.templateResourcePath(organizationId).folder}/${key}}`,
      file,
      contentType
    });

  _upload({
    key,
    file,
    contentType,
    rawString
  }: RawUpload) {
    return new Promise((resolve, reject) => {
      this.s3.upload({
        Key: `${key}`,
        Bucket: process.env.S3_BUCKET,
        Body: rawString ? file : createReadStream(file),
        ACL: 'public-read',
        ContentType: contentType
      }, (err, result) => {
        if (err) {
          return reject(err);
        }
        const mResult = { ...result };
        mResult.Location = this.locationToCloudfront(mResult.Location);

        return resolve(mResult);
      });
    });
  }
}

export default new S3();
