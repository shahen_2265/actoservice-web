import config from 'config';
import isEmpty from 'lodash/isEmpty';

const STORAGE_API = config.s3.host;

class _S3 {
  templateResourcePath = (organizationId: number, includePrefix = false) => {
    if (isEmpty(organizationId)) {
      throw new Error('Organization Id is required');
    }
    const { folder: templateFolder } = this.templatePath(organizationId, includePrefix);
    const folder = `${templateFolder}/resources`;

    return {
      folder: includePrefix ? folder : folder.replace(`${STORAGE_API}/`, ''),
    };
  };

  resourcesPath = (apiKey: string, includePrefix = false) => {
    if (isEmpty(apiKey)) {
      throw new Error('No Developer Api Key');
    }
    const folder = `${STORAGE_API}/resources/draft/${apiKey}`;
    return {
      folder: includePrefix ? folder : folder.replace(`${STORAGE_API}/`, ''),
    };
  };
  templatePath = (orgId, includePrefix = false) => {
    const folder = `${STORAGE_API}/templates/o/${orgId}`;
    const file = `${folder}/template.json`;

    return {
      folder: includePrefix ? folder : folder.replace(`${STORAGE_API}/`, ''),
      file: includePrefix ? file : file.replace(`${STORAGE_API}/`, ''),
    };
  }
  themePath = (themeName, includePrefix = false) => {
    const folder = `${STORAGE_API}/themes/${themeName}`;
    const scheme = `${folder}/scheme.json`;

    return {
      folder: includePrefix ? folder : folder.replace(`${STORAGE_API}/`, ''),
      scheme: includePrefix ? scheme : scheme.replace(`${STORAGE_API}/`, ''),
    };
  };
}

export const s3Util = new _S3();
