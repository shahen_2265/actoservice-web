/**
 * @flow
 */
import get from 'lodash/get';
import includes from 'lodash/includes';
import haveChildObject from './haveChildObject';
import defaultValueForType from './defaultValueForType';

export default function generateTemplate(scheme: {}) {
  const generated = {};
  const keys = Object.keys(scheme);

  if (includes(get(scheme, 'type'), 'array')) {
    return get(scheme, 'defaultValue');
  }

  keys.forEach((key) => {
    const obj = scheme[key];
    if (haveChildObject(obj)) {
      generated[key] = generateTemplate(obj);
    } else {
      generated[key] = defaultValueForType(obj);
    }
  });

  return generated;
}
