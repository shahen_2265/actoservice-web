/**
 * eslint no-param-reassign: 0
 * @flow
*/

import isNil from 'lodash/isNil';
import isObject from 'lodash/isObject';
import includes from 'lodash/includes';
import config from 'config';
import haveChildObject from './haveChildObject';
import isArrayValueCompatable from './isArrayValueCompatable';

type HoistError = {
  message?: string
};

export default function isSchemeValid(scheme: {}, hoistError: HoistError = {}) {
  const keys = Object.keys(scheme);

  return !keys.some((key) => {
    const currValue = scheme[key];

    if (isNil(currValue)) {
      // eslint-disable-next-line
      hoistError.message = `Invalid scheme found: value is nil, ${JSON.stringify(currValue)}`;
      return true;
    }
    if (!isObject(currValue)) {
      // eslint-disable-next-line
      hoistError.message = `Invalid scheme found: value is not a object, ${key} - ${JSON.stringify(currValue)}`;
      return true;
    }
    if (haveChildObject(currValue)) {
      if (includes(currValue.type, 'array')) {
        return !isArrayValueCompatable(currValue);
      }
      return !isSchemeValid(currValue, hoistError);
    }

    const isTypeNil = isNil(currValue.type);
    const containsMetadata = !!currValue.__title__;

    const isTypeAcceptable = config.themes.types.includes(currValue.type);

    if (isTypeNil) {
      // eslint-disable-next-line
      hoistError.message = `
        Invalid scheme found: type is not defined,
        ${JSON.stringify(currValue)}
      `;
    }
    if (!isTypeAcceptable) {
      // eslint-disable-next-line
      hoistError.message = `
        Invalid scheme found: unacceptable type received,
        ${JSON.stringify(currValue)}
      `;
    }
    if (!containsMetadata) {
      // eslint-disable-next-line
      hoistError.message = `
        Invalid scheme found: Metadata not found(ex. title),
        ${JSON.stringify(currValue)}
      `;
    }
    return isTypeNil || !isTypeAcceptable || !containsMetadata;
  });
}
