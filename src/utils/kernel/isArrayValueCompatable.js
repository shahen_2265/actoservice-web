/**
 * @flow
 */
import get from 'lodash/get';
import isArray from 'lodash/isArray';

const isArrayValueCompatable = (chunk: {}) =>
  isArray(get(chunk, 'value', get(chunk, 'defaultValue')));

export default isArrayValueCompatable;
