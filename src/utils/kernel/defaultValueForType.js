/**
 * @flow
 */
import get from 'lodash/get';
import config from 'config';

type KeyType = {
  type: string,
  defaultValue: ?string
};

const defaultValueForType = (key: KeyType) =>
  key.defaultValue || get(config.themes.defaultValues, key.type);


export default defaultValueForType;
