/**
 * @flow
 */
import isObject from 'lodash/isObject';

export default function haveChildObject(value: {}) {
  return Object.keys(value).some(key => isObject(value[key]));
}
