/**
 * @flow
*/

import isNil from 'lodash/isNil';
import get from 'lodash/get';
import keyify from './keyify';

export const isTypeCompatable = (type: ?string, value: any) => {
  // TODO
  if (type === null) {
    return false;
  }
  return !!value;
};

export default function isTemplateCompatableWithTheme(template: {}, theme: {}) {
  const templateKeys = keyify(template);

  return templateKeys.every((key) => {
    const keyExists = !isNil(get(theme, key));
    const typeCompatable = isTypeCompatable(get(theme, `${key}.type`), get(template, key));

    return keyExists && typeCompatable;
  });
}

