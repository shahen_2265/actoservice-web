/**
 * @flow
 */
import set from 'lodash/set';
import get from 'lodash/get';
import keyify from './keyify';

const mixTemplateScheme = (templateJSON: {}, scheme: {}): {} =>
  keyify(templateJSON)
    .reduce((all, currPath: string) => {
      set(all, `${currPath}.value`, get(templateJSON, `${currPath}`));
      set(all, `${currPath}.type`, get(scheme, `${currPath}.type`));
      set(all, `${currPath}.title`, get(scheme, `${currPath}.__title__`));
      return all;
    }, {});

export default mixTemplateScheme;
