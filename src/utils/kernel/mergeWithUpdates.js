/**
 * @flow
 */

import get from 'lodash/get';
import set from 'lodash/set';
import mergeWith from 'lodash/mergeWith';
import keyify from './keyify';

export default function mergeWithUpdates(template, updates) {
  const updatesKeys = keyify(updates);
  const escapedUpdates = updatesKeys.reduce((upd, key) => {
    const escapedKey = key.replace('.value', '');
    const preUpdate = { ...upd };
    set(preUpdate, escapedKey, get(updates, key));
    return preUpdate;
  }, {});
  return mergeWith(
    {}, template, escapedUpdates,
    (objVal, sourceVal) =>
      (Array.isArray(sourceVal) ? sourceVal : undefined)
  );
}
