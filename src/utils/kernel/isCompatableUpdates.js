/**
 * @flow
 */
import keyify from './keyify';

export default function isCompatableUpdates(template: {}, updates: {}) {
  const templateKeys = keyify(template);
  const updatesKeys = keyify(updates);

  const escapedKeys = updatesKeys
    .map(key => key.replace('.value', ''));

  return escapedKeys.every(key => templateKeys.includes(key));
}
