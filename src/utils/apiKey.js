/**
 * @flow
*/

import crypto from 'crypto';
import isEmpty from 'lodash/isEmpty';
import isString from 'lodash/isString';
import size from 'lodash/size';
// $FlowFixMe
import SystemError from 'errors/SystemError';
import config from 'config';

const atob = a =>
  Buffer.from(a, 'base64').toString('binary');

const btoa = b =>
  Buffer.from(JSON.stringify(b)).toString('base64');

type ParseAPIKey = {
  shallowValid: boolean,
  payload?: {
    [key: string]: *
  }
};
export const parseAPIKey = (key: string): ParseAPIKey => {
  const splitted = key.split(':');
  let shallowValid = size(splitted) === 3;

  let payload = null;
  try {
    payload = JSON.parse(atob(splitted[1]));
  } catch (e) {
    shallowValid = false;
  }

  return {
    payload,
    shallowValid
  };
};

export const validateApiKeyBasic = (key: string) => {
  if (isEmpty(key)) {
    return { err: 'Key is empty' };
  }
  if (!isString(key)) {
    return { err: 'Key is not a string' };
  }
  const splitted = key.split(':');

  if (size(splitted) !== 3) {
    return { err: 'Key is invalid' };
  }
  try {
    JSON.parse(atob(splitted[1]));
  } catch (e) {
    return { err: 'Key contains invalid payload' };
  }

  return { err: null };
};

export const generateApiKey = (data: {}) => {
  const token = crypto.randomBytes(19).toString('hex');
  const tokenLength = size(token);
  const encoded = btoa(data);

  const { requiredApiKeyLength } = config.developer;

  if (tokenLength !== requiredApiKeyLength) {
    throw new SystemError(`Token should have ${requiredApiKeyLength} but received ${tokenLength}`);
  }
  const part1 = token.substr(0, tokenLength / 2);
  const part2 = token.substr(tokenLength / 2);

  return `${part1}:${encoded}:${part2}`;
};
