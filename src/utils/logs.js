// @flow

import debug from 'debug';

const appName = 'actoservice';

export default function createLogs(moduleName: string) {
    return debug(`${appName}:${moduleName}`);
}
