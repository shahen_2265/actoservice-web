/**
 * @flow
 */

import trim from 'lodash/trim';
import toLower from 'lodash/toLower';
import toUpper from 'lodash/toUpper';
import isNil from 'lodash/isNil';

type Options = {
    lowerCase?: Array<string>,
    upperCase?: Array<string>
}

const dummyOptions: Options = {
  lowerCase: [],
  upperCase: []
};

type DataType = {
  [key: string]: *
};

export default function processData(data: DataType, options: Options = dummyOptions) {
  if (isNil(data)) {
    return null;
  }
  return Object.keys(data)
    .reduce((obj, key) => {
      const shouldUpperCase = options.upperCase && options.upperCase.includes(key);
      const shouldLowerCase = options.lowerCase && options.lowerCase.includes(key);


      let value = data[key];

      if (shouldUpperCase) {
        value = toUpper(value);
      }
      if (shouldLowerCase) {
        value = toLower(value);
      }

      return {
        ...obj,
        [key]: trim(value)
      };
    }, {});
}
