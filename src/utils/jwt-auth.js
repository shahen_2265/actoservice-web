import jwt from 'jsonwebtoken';
import isNil from 'lodash/isNil';
import isEmpty from 'lodash/isEmpty';

const signature = process.env.TOKEN_SIGNATURE;
const secret = process.env.JWT_SECRET;

export function developerOnly({ throwError, userRequired } = {}) {
  return async function middleware(ctx, next) {
    const key = ctx.getApiKey();

    if (isEmpty(key)) {
      ctx.throw(403, 'Developer API key required');
    }

    const isValid = await ctx.validateApiKey({ throwError, userRequired });

    if (!isValid) {
      ctx.throw(403, 'Developer API key is invalid');
    }

    await next();
  };
}

export function isTokenValid(token: string): {} | boolean {
  try {
    const signedToken = token.split(`${signature} `)[1];
    const { user } = jwt.verify(signedToken, secret);
    return user;
  } catch (e) {
    return false;
  }
}

export async function parseToken(ctx, next) {
  const token = ctx.getToken();
  if (!token) {
    return next();
  }
  try {
    const signedToken = token.split(`${signature} `)[1];
    const { user, organizations } = jwt.verify(signedToken, secret);
    ctx.state.user = user;
    ctx.state.organizations = organizations;
  } catch (e) {
    // pass
  }
  return next();
}

export async function authRequired(ctx, next) {
  const token = ctx.getToken();
  if (isNil(token)) {
    ctx.throw(401);
    return;
  }
  try {
    const signedToken = token.split(`${signature} `)[1];

    const { user, organizations } = jwt.verify(signedToken, secret);

    ctx.state.organizations = organizations;
    ctx.state.user = user;
  } catch (e) {
    ctx.throw(401);
  }
  if (!isNil(next)) {
    await next();
  }
}

export function generateToken(ctx, user) {
  const token = jwt.sign({ user }, process.env.JWT_SECRET, {
    expiresIn: '100d'
  });
  const signedToken = [signature, token].join(' ');

  ctx.setToken(signedToken);
}
