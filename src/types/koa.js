export type Context = {
  state: {
    [key: string]: any
  },
  request: any,
  response: any
};
