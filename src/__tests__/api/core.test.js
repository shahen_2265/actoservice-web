import axios from 'axios';

describe('Core API', () => {
  describe('Actoservice HTML', () => {
    let result = null;
    const findVarsRegexp = /var\sisRoot\s=\strue/;

    beforeAll(async () => {
      result = await axios.get(HOST);
    });

    it('should send html', () => {
      expect(result.status).toBe(200);
    });
    it('should inject variables in the head', () => {
      expect(findVarsRegexp.test(result.data)).toBe(true);
    });
  });
});

