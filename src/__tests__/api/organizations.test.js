import omit from 'lodash/omit';
import pick from 'lodash/pick';
import axios from 'axios';

const ORG_API = `${API}/organizations`;

const data = {
  name: 'Coffessta',
  path: 'coffessta',
  phone: '+37455095466'
};

describe('Organizations API', () => {
  let token = null;

  beforeAll(async () => {
    const response = await createUser();
    token = response.headers.authorization.substr();
  });

  afterAll(async () => {
    await removeUser(token);
  });

  describe(ORG_API, () => {
    it('#POST -> Should fail to create organization if user is not authenticated', async () => {
      try {
        await axios.post(ORG_API, data);
        expect(1).toBe(2);
      } catch (e) {
        expect(e.response.status).toBe(401);
      }
    });
    it('#POST -> Should fail to create organization if `path` is not specified', async () => {
      try {
        await axios.post(ORG_API, omit(data, ['path']), {
          headers: {
            Authorization: token
          }
        });
        expect(1).toBe(2);
      } catch (e) {
        expect(e.response.status).toBe(422);
      }
    });
    it('#POST -> Should create organization successfuly', async () => {
      try {
        const res = await axios.post(ORG_API, data, {
          headers: {
            Authorization: token
          }
        });

        expect(res.status).toBe(201);
        expect(omit(res.data, ['createdAt', 'updatedAt', 'id'])).toMatchSnapshot();
      } catch (e) {
        expect('Shouldn fail').toBe(`failed with error - ${e}`);
      }
    });
  });
  describe(`${ORG_API}?subdomain=SUBDOMAIN`, () => {
    it('SUBDOMAIN=coffessta: should send 422 request indicates that this organization exists', async () => {
      try {
        await axios.get(`${ORG_API}?subdomain=coffessta`);
        expect('There should be an error').toBe('not failed');
      } catch (e) {
        expect(e.response.status).toBe(422);
      }
    });
    it('SUBDOMAIN=: should send 400 if subdomain is empty', async () => {
      try {
        await axios.get(`${ORG_API}?subdomain=`);
        expect('There should be an error').toBe('not failed');
      } catch (e) {
        expect(e.response.status).toBe(401);
      }
    });
    it('SUBDOMAIN= <script>something</test>: should send 401 if subdomain contains special characters', async () => {
      try {
        await axios.get(`${ORG_API}?subdomain=<script>something</test>`);
        expect('There should be an error').toBe('not failed');
      } catch (e) {
        expect(e.response.status).toBe(422);
      }
    });
    it('SUBDOMAIN=notfoundsubdomain: should send 200 status if organization doesnt exists', async () => {
      const response = await axios.get(`${ORG_API}?subdomain=notfoundsubdomain`);
      expect(response.status).toBe(200);
    });
  });

  describe(`${ORG_API}/coffessta/themes?object=scheme.json`, () => {
    it('should fail if object is not found', () => {
      
    });
  });
});
