import axios from 'axios';
import omit from 'lodash/omit';
import pick from 'lodash/pick';

const SIGNUP_API = `${API}/auth/signup`;
export const SIGNIN_API = `${API}/auth/signin`;

describe('Users API', () => {
  let token = null;

  afterAll(async () => {
    await removeUser(token);
  });

  describe('/auth/signin', () => {
    it('#POST -> should fail to sign in', async () => {
      try {
        const correctCredentials = pick(USER_CREDENTIALS, ['email', 'password']);
        await axios.post(SIGNIN_API, correctCredentials);
        expect(1).toBe(2);
      } catch (e) {
        expect(e.response.status).toBe(404);
      }
    });
  });

  describe('/auth/signup', () => {
    it('#POST -> should fail to signup without name', async () => {
      try {
        await axios.post(SIGNUP_API, omit(USER_CREDENTIALS, ['name']));
        expect(1).toBe(2);
      } catch (e) {
        expect(e.response.status).toBe(422);
      }
    });

    it('#POST -> should fail to signup without any data', async () => {
      try {
        await axios.post(SIGNUP_API);
        expect(1).toBe(2);
      } catch (e) {
        expect(e.response.status).toBe(422);
      }
    });
    it('#POST -> should signup successfully', async () => {
      const response = await createUser();

      const newToken = response.headers.authorization;
      expect(newToken).toBeDefined();
      expect(response.status).toBe(201);
      expect(omit(response.data, ['createdAt', 'updatedAt', 'id']))
        .toEqual(omit(USER_CREDENTIALS, ['password']));

      token = newToken;
    });
  });
});
