/**
 * @flow
 */
import path from 'path';

type Err = {
  status: number,
  message: string
};

const isProd = process.env.NODE_ENV === 'production';

export const ASString = 'AS:string';
export const ASNumber = 'AS:number';
export const ASColor = 'AS:color';
export const ASPhone = 'AS:phone';
export const ASImage = 'AS:image';
export const ASBool = 'AS:bool';

export const ASArrayImage = 'AS:array:image';
export const ASArrayString = 'AS:array:string';
export const ASArrayNumber = 'AS:array:number';

const defaultValues = {
  [ASString]: '',
  [ASNumber]: 0,
  [ASColor]: '#000000',
  [ASPhone]: '0',
  [ASImage]: null,
  [ASBool]: false,

  [ASArrayImage]: [],
  [ASArrayString]: [],
  [ASArrayNumber]: []
};

const valueTypes: string[] = Object.keys(defaultValues);

const {
  CLOUD_FRONT_API,
  S3_BUCKET,
  S3_API
} = process.env;

if (!CLOUD_FRONT_API) {
  throw new Error('CLOUD_FRONT_API env variable desont set');
}
if (!S3_BUCKET) {
  throw new Error('S3_BUCKET env variable desont set');
}
if (!S3_API) {
  throw new Error('S3_API env variable desont set');
}

export default {
  isProd,
  web: {
    clientFolderPath: path.join(process.cwd(), 'public'),
    actoserviceFolderPath: path.join(process.cwd(), 'actoservice-build')
  },
  helmet: {
    frameguard: isProd
  },
  errorParser: (err: Err) => ({
    status: err.status,
    message: err.message ? err.message.split('\n') : 'something went wrong',
    success: false
  }),
  overloadProtection: {
    production: isProd,
    clientRetrySecs: 1,
    sampleInterval: 5,
    maxEventLoopDelay: 100,
    maxHeapUsedBytes: 0,
    maxRssBytes: 0,
    errorPropagationMode: false
  },
  developer: {
    requiredApiKeyLength: 38,
    payloadKeys: ['id', 'email']
  },
  organizations: {
    // eslint-disable-next-line
    subdomainInvalidator: /[ !@#$%^&*()+\=\[\]{};':"\\|,.<>\/?]/,
    expireDateFormat: 'YYYY-MM-DD',
    blacklistedSubdomains: [
      'admin',
      'api',
      'www'
    ]
  },
  cors: {
    keepHeadersOnError: true,
    allowHeaders: [
      process.env.JWT_COOKIE,
      'Accept',
      'Accept-Language',
      'Content-Language',
      'Content-Type',
      process.env.API_KEY_HEADER
    ],
    exposeHeaders: [
      process.env.JWT_COOKIE,
      // process.env.API_KEY_HEADER
    ],
    origin: undefined
  },
  compression: {
    filter: (contentType: string) => {
      return (/json|text|javascript|css|font|svg/).test(contentType);
    }
  },
  bodyParser: {
    formidable: { uploadDir: __dirname }
  },
  cache: {
    developerApiKey: 'developer_api_key',
    organizationKey: 'organization',
    organizationThemeMixedTemplateKey: 'organizationMixedTemplate'
  },
  db: {
    logging: false
  },
  s3: {
    host: isProd
      ? `${S3_API}/${S3_BUCKET}` // CLOUD_FRONT_API
      : `${S3_API}/${S3_BUCKET}`
  },
  session: {
    httpOnly: true,
    signed: true,
    maxAge: 86400000,
    key: '__acto__sess__'
  },
  themes: {
    defaultId: 1,
    types: valueTypes,
    defaultValues
  }
};
