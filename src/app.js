import http from 'http';
import connectDB from 'lib/database';
import createLog from 'utils/logs';

import { attachSocket } from 'lib/socket';

const log = createLog('app');

export async function start() {
  log('Starting the app');

  const port = process.env.PORT;
  await connectDB();
  const app = require('./lib/koa').default;
  const server = http.createServer(app.callback());

  attachSocket(app, server);
  server.listen(port);

  log('listening on port ', port);
}
