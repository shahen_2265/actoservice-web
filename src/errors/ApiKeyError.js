/**
 * @flow
*/

export default class ApiKeyError extends Error {
  constructor(message) {
    super(message);
    this.name = 'ApiKeyError';
    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor);
      return;
    }
    this.stack = (new Error(message)).stack;
  }
}
