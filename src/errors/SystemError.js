/**
 * @flow
 */

export default class SystemError extends Error {
  constructor(message) {
    super(message);
    this.name = 'SystemError';
    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor);
      return;
    }
    this.stack = (new Error(message)).stack;
  }
}
