import { URL } from 'url';

const previewPath = '/_/preview';

export function isPreviewPage(ctx) {
  if (!ctx.headers.referer) {
    return false;
  }
  const url = new URL(ctx.headers.referer);

  return url.pathname.toLowerCase() === previewPath;
}

export function getPreviewTheme(ctx) {
  if (!ctx.headers.referer) {
    return null;
  }
  const url = new URL(ctx.headers.referer);

  return url.searchParams.get('theme');
}

export function getPreviewThemeForRenderer(ctx) {
  return ctx.query.theme;
}

export function isPreviewPageForRenderer(ctx) {
  return ctx.path.toLowerCase() === previewPath
    && !!ctx.query.theme;
}
