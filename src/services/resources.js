/**
 * @flow
*/
import multiparty from 'multiparty';
import mime from 'mime-types';
import s3 from 'utils/s3';

type ReturnValue = Promise<Array<{
  url: string,
  key: string
}>>;

type UploadOptions = {
  isTheme?: boolean,
  organizationId?: number
};

export async function uploadResource(ctx: any, options: UploadOptions = {}): ReturnValue {
  const resourcesForm = new multiparty.Form();

  return new Promise((resolve, reject) => {
    resourcesForm.parse(ctx.req, async (err, fields, files) => {
      if (err) {
        return reject(err);
      }
      const assets = Object.keys(files)
        .map(fileKey => ({
          file: files[fileKey][0].path,
          key: files[fileKey][0].fieldName
        }));


      const s3Fn = options.isTheme
        ? 'uploadResource'
        : 'uploadTemplateResource';

      const assetsUpload = assets.map(({ key, file }) =>
      // $FlowFixMe
        s3[s3Fn]({
          file,
          key,
          contentType: mime.lookup(file),
          apiKey: ctx.getApiKey(),
          organizationId: options.organizationId
        }));

      return Promise.all(assetsUpload)
        .then(s3Responses =>
          s3Responses.map(({ Location }, i) => ({
            url: Location,
            key: assets[i].key
          })))
        .then(resolve)
        .catch(reject);
    });
  });
}
