/**
 * @flow
 */
import Sequelzie from 'sequelize';
import glob from 'glob';
import path from 'path';
import createLog from 'utils/logs';
import config from 'config';

type ExportDatabase = {
  models: {
    [key: string]: *
  },
  sequelize?: typeof Sequelzie
};

/**
 * @public
 */
export const database: ExportDatabase = {
  models: {}
};

const log = createLog('sequelize');

export const sequelize = new Sequelzie({
  database: 'actoservice',
  username: 'root',
  password: 'root',

  logging: config.db.logging,
  host: process.env.POSTGRESQL_HOST,
  port: process.env.POSTGRESQL_PORT,
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

database.sequelize = sequelize;

const prodModels = process.env.NODE_ENV === 'production';

const modelsPattern = prodModels
  ? path.resolve('dist/modules/**/*.model.js')
  : path.resolve('src/modules/**/*.model.js');

async function initializeModels() {
  log('Initializing models');
  glob.sync(modelsPattern)
    .forEach((modelPath) => {
      // $FlowFixMe, eslint-disable-next-line
      const mM = require(modelPath);
      const model = sequelize.import(modelPath, mM);

      database.models[model.name] = model;
    });
  Object.keys(database.models)
    .forEach((modelName) => {
      const model = database.models[modelName];
      if ('associate' in model) {
        model.associate(database.models);
      }
    });
  log('Initialzied !!!');
}

export default async function sync() {
  let attempt = 0;
  const maxAttempts = 30;

  function connect(resolve, reject) {
    sequelize.authenticate()
      .then(() => log('Connection Established'))
      .then(initializeModels)
      .then(() => log('Syncing schemas'))
      .then(() => sequelize.sync({ force: false }))
      .then(resolve)
      .catch((e) => {
        if (!e.name === 'SequelizeConnectionRefusedError') {
          return reject(e);
        }
        if (attempt === maxAttempts) {
          log('Max attempt');
          return reject();
        }
        attempt += 1;

        log(`Retrying: attempt ${attempt}/${maxAttempts}`);

        return setTimeout(() => connect(resolve, reject), 1000);
      });
  }

  return new Promise((resolve, reject) =>
    connect(resolve, reject));
}
