/**
 * @flow
 */
import isNumber from 'lodash/isNumber';
import cache from './Redis';

class SessionStore {
  get = async (key: string) => {
    const json = await cache.get(key);
    try {
      return JSON.parse(json);
    } catch (e) {
      return null;
    }
  }

  set = async (key: string, sess: {}, _ttl?: number) => {
    const stringified = JSON.stringify(sess);
    let ttl = _ttl;
    if (isNumber(ttl)) {
      ttl /= 1000;
      await cache._setex(key, ttl, stringified);
      return;
    }
    await cache.set(key, stringified);
  }

  destroy = async (key) => {
    await cache.remove(key);
  }

  quit = async () => {
    // TODO: Need to think about this case
  }
}

export default SessionStore;
