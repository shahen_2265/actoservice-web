/**
 * @flow
 */
import redis from 'redis';
import { promisify } from 'util';
import createLog from 'utils/logs';
import config from 'config';

const log = createLog('redis');

class CacheService {
  client: redis.RedisClient;
  _get: (key: string) => Promise<string>;
  _set: (key: string, value: string) => Promise<string>;
  _remove: (key: string) => Promise<string>;

  _hget: (hash: string, key: string) => Promise<string>;
  _hset: (hash: string, key: string, value: string) => Promise<string>;
  _hdel: (hash: string, key: string) => Promise<string>;

  constructor() {
    this.client = redis.createClient({
      host: process.env.REDIS_HOST,
      port: process.env.REDIS_PORT
    });

    this.client.on('error', (err) => {
      log(`error: ${err}`);
    });

    this._get = promisify(this.client.get).bind(this.client);
    this._hget = promisify(this.client.hget).bind(this.client);
    this._hdel = promisify(this.client.hdel).bind(this.client);
    this._hset = promisify(this.client.hset).bind(this.client);
    this._set = promisify(this.client.set).bind(this.client);
    this._remove = promisify(this.client.del).bind(this.client);
    this._setex = promisify(this.client.setex).bind(this.client);
  }

  set(key: string, value: string) {
    let val = value;
    if (typeof value !== 'string') {
      val = JSON.stringify(value);
    }
    return this._set(key, val);
  }
  get(key: string): Promise<*> {
    return this._get(key)
      .then((res) => {
        try {
          return JSON.parse(res);
        } catch (e) {
          return res;
        }
      });
  }

  getDeveloperApiKey = (id: string) =>
    this._hget(config.cache.developerApiKey, id);

  setDeveloperApiKey = (id: string, apiKey: string) =>
    this._hset(config.cache.developerApiKey, id, apiKey);

  setOrganizationTheme = (organizationPath: string, themePath: string) =>
    this._hset(config.cache.organizationKey, organizationPath, themePath);

  getOrganizationTheme = (organizationPath: string) =>
    this._hget(config.cache.organizationKey, organizationPath);

  getThemeMixedTemplate = (organizationPath: string) =>
    this._hget(
      config.cache.organizationThemeMixedTemplateKey,
      organizationPath
    )
      .then(res => res && JSON.parse(res));

  setThemeMixedTemplate = (organizationPath: string, mixedObject: {}) =>
    this._hset(
      config.cache.organizationThemeMixedTemplateKey,
      organizationPath,
      JSON.stringify(mixedObject)
    );

  remove(key: string): Promise<*> {
    return this._remove(key);
  }
}

const cache = new CacheService();

export default cache;

export function attachRedis(app: any) {
  log('Initialize Redis Cache service');
  // eslint-disable-next-line
  app.cache = cache;
  // eslint-disable-next-line
  app.context.cache = cache;
}
