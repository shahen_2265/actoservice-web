/**
 * @flow
 */
import Koa from 'koa';
import koaError from 'koa-json-error';
import koaBody from 'koa-body';
import overloadProtection from 'overload-protection';
import helmet from 'koa-helmet';
import koaLogger from 'koa-logger';
import compress from 'koa-compress';
import cors from '@koa/cors';
import session from 'koa-session';
import proxy from 'middlewares/proxy';
import createLog from 'utils/logs';
import attachModels from 'middlewares/attachModels';
import jwtMethods from 'middlewares/jwtMethods';
import querySanitize from 'middlewares/querySanitize';
import parseOrganization from 'middlewares/parseOrganization';
import developerMethods from 'middlewares/developer';
import { parseToken } from 'utils/jwt-auth';
import config from 'config';

import { attachRedis } from './Redis';
import { attachRoutes } from './router';
import SessionStore from './SessionStore';

const log = createLog('koa');
const app = new Koa();

app.keys = [process.env.SIGNED_KEYS];

log('initialize middlewares');

if (!config.isProd) {
  // $FlowFixMe
  config.cors.origin = '*';
}

const sessionConfig = {
  ...config.session,
  store: new SessionStore()
};

app.use(session(sessionConfig, app));
app.use(koaError(config.errorParser));
app.use(koaLogger());
app.use(overloadProtection('koa', config.overloadProtection));
app.use(cors(config.cors));
app.use(helmet(config.helmet));
app.use(koaBody(config.bodyParser));
app.use(compress(config.compression));

app.use(querySanitize);
app.use(jwtMethods);
app.use(attachModels);
app.use(parseToken);
app.use(parseOrganization);
app.use(developerMethods);
app.use(proxy);

attachRedis(app);
attachRoutes(app);

export default app;
