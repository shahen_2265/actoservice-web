import glob from 'glob';
import path from 'path';
import createLog from 'utils/logs';

const log = createLog('router');

const isProd = process.env.NODE_ENV === 'production';

const routesPattern = isProd
  ? 'dist/modules/**/*.routes.js'
  : 'src/modules/**/*.routes.js';

const routes = path.join(process.cwd(), routesPattern);
const prefix = `/api/${process.env.API_VERSION}`;

export function attachRoutes(app) {
  log('Initialize Routes');
  glob.sync(routes)
    .forEach(routePath =>
      require(routePath).attach(app, prefix));
}

