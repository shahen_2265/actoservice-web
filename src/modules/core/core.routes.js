/**
 * @flow
 */

import Router from 'koa-router';
import createLog from 'utils/logs';

import { renderPage } from './core.handlers';

const log = createLog('core:routes');

export function attach(app: any) {
  log('Initialize Core routes');

  const router = new Router({
    prefix: ''
  });

  router.get(/^((?!\/api))/i, renderPage);

  app.use(router.routes());
  app.use(router.allowedMethods());
}
