/**
 * @flow
 */

import path from 'path';
import get from 'lodash/get';
import isNil from 'lodash/isNil';

import { promisify } from 'util';
import { readFile as rf } from 'fs';
// $FlowFixMe
import type { Context } from 'types';

import { isPreviewPageForRenderer, getPreviewThemeForRenderer } from 'services/preview';
import s3 from 'utils/s3';
import { s3Util } from 'utils/resources';
import mixTemplateScheme from 'utils/kernel/mixTemplateScheme';

import config from 'config';

const readFile = promisify(rf);

type PassVariables = {
  isRoot: boolean,
  __ACTOSERVICE__SCHEME__?: string
};

function template(content: string, variables: PassVariables): string {
  const headClosing = '</head>';
  const splitedByHead = content.split(headClosing);
  const variableKeys = Object.keys(variables);

  const stringyfiedVariables = variableKeys.map(key =>
    // $FlowFixMe
    `var ${key} = ${variables[key]};`);

  const script = `
    <script>
      ${stringyfiedVariables.join('\n')}
    </script>
  `;

  const newContent = [
    splitedByHead[0],
    script,
    headClosing,
    splitedByHead[1]
  ];

  return newContent.join('');
}

const {
  clientFolderPath,
  actoserviceFolderPath
} = config.web;

export async function renderPage(ctx: Context) {
  const { organizationSubdomain } = ctx.state;
  const isPreview = isPreviewPageForRenderer(ctx);
  const previewTheme = getPreviewThemeForRenderer(ctx);

  const publicPath = (isPreview || organizationSubdomain)
    ? clientFolderPath
    : actoserviceFolderPath;

  const varialbes: PassVariables = {
    isRoot: ctx.state.isActoservice
  };

  if (organizationSubdomain) {
    let theme = await ctx.cache.getOrganizationTheme(organizationSubdomain);
    if (!theme) {
      // TODO: Render Not found page
      const _themeQuery = await ctx.models.organization.findOne({
        attributes: ['id'],
        include: [{
          model: ctx.models.template,
          attributes: ['id'],
          include: [{ model: ctx.models.theme, attributes: ['name'] }]
        }],
        where: { path: organizationSubdomain }
      });
      console.log(_themeQuery);
      theme = get(_themeQuery, 'template.theme.name');
      if (isNil(theme)) {
        ctx.throw(400, 'page not found');
      }
      await ctx.cache.setOrganizationTheme(organizationSubdomain, theme);
    }
    let themeMixedTemplate = await ctx.cache.getThemeMixedTemplate(organizationSubdomain);

    if (!themeMixedTemplate) {
      const org = await ctx.models.organization.findOne({
        attributes: ['id'],
        where: { path: organizationSubdomain }
      });

      const [scheme, templateJSON] = await Promise.all([
        // $FlowFixMe
        s3.getObject(s3Util.themePath(theme).scheme),
        // $FlowFixMe
        s3.getObject(s3Util.templatePath(org.id).file)
      ]);

      if (scheme.err || templateJSON.err) {
        console.log(scheme.err, templateJSON.err);
        ctx.throw(400);
      }
      const mixed = mixTemplateScheme(templateJSON.data, scheme.data);
      await ctx.cache.setThemeMixedTemplate(organizationSubdomain, mixed);
      themeMixedTemplate = mixed;
    }

    varialbes.__ACTOSERVICE__SCHEME__ = JSON.stringify(themeMixedTemplate);
  }

  if (isPreview) {
    const scheme = await s3.getObject(s3Util.themePath(previewTheme).scheme);

    const { err, data } = scheme;
    if (err) {
      ctx.throw(400, err);
    }
    varialbes.__ACTOSERVICE__SCHEME__ = JSON.stringify(data);
  }

  const indexPath = path.join(publicPath, 'index.html');
  return readFile(indexPath)
    .then((content) => {
      const contentString = content.toString();

      const tmp = template(contentString, varialbes);

      ctx.body = tmp;
      ctx.type = 'text/html';
    });
}
