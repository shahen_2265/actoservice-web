/**
 * @flow
*/

import { Op } from 'sequelize';

type FetchCategoriesQuery = {
  limit: number,
  where?: {
    id: {
      [key: string]: number
    }
  }
};

export async function getCategories(ctx: any) {
  const { prev, limit } = ctx.query;
  const query: FetchCategoriesQuery = { limit };

  if (prev) {
    query.where = {
      id: {
        [Op.gt]: prev
      }
    };
  }

  ctx.body = await ctx.models.category.findAll(query);
}
