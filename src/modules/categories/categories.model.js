/**
 * @flow
 */
import createLog from 'utils/logs';
import Sequelize from 'sequelize';

const log = createLog('categories:model');

log('Initialize categories model');

module.exports = (sequelize: Sequelize, DataTypes: Sequelize.DataTypes) => {
  const categoriesScheme = sequelize.define('category', {
    name_en: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name_ru: {
      type: DataTypes.STRING,
      allowNull: true
    },
    name_hy: {
      type: DataTypes.STRING,
      allowNull: true
    }
  });

  categoriesScheme.associate = function (db) {
    this.hasMany(db.theme);
  };

  return categoriesScheme;
};
