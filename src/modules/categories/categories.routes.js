import Router from 'koa-router';
import Boom from 'boom';
import createLog from 'utils/logs';
import { getCategories } from './categories.handlers';

const log = createLog('categories:routes');

export function attach(app, prefix) {
  const categoriesRoutePrefix = `${prefix}/categories`;

  const router = new Router({
    prefix: categoriesRoutePrefix
  });

  router.get('categories', '/', getCategories);

  app.use(router.routes());
  app.use(router.allowedMethods({
    notImplemented: () => new Boom.notImplemented(),
    methodNotAllowed: () => new Boom.methodNotAllowed()
  }));

  log('categories routes initialized');
}
