/**
 * @flow
 */
import get from 'lodash/get';
import createLog from 'utils/logs';
import s3 from 'utils/s3';
import config from 'config';
import { createReadStream } from 'fs';
import isSchemeValid from 'utils/kernel/isSchemeValid';
import Sequelize from 'sequelize';

const log = createLog('themes:model');

log('Initialize Themes model');

const readSchemeAndValidate = (path) => {
  return new Promise((resolve, reject) => {
    const readStream = createReadStream(path);
    const chunks = [];
    readStream.on('data', chunk => chunks.push(chunk.toString()));

    readStream.on('end', () => {
      try {
        const obj = JSON.parse(chunks.join(''));
        const hoistErrors = {};
        const isValid = isSchemeValid(obj, hoistErrors);

        if (isValid) {
          return resolve();
        }
        // eslint-disable-next-line
        const err = ''
          + 'actoservice.json is invalid \n'
          + hoistErrors.message;

        return reject(new Error(err));
      } catch (e) { return reject(e); }
    });
  });
};

async function beforeCreate(data, { files }) {
  try {
    await readSchemeAndValidate(files.configMap.file);

    const [
      source,
      scheme,
      imageUrl
    ] = await s3.uploadTheme(files);

    // eslint-disable-next-line
    data.source = source.Location;
    // eslint-disable-next-line
    data.scheme = scheme.Location;
    // eslint-disable-next-line
    data.imageUrl = get(imageUrl, 'Location');
  } catch (e) { throw new Error(e); }
}

function isFree() {
  return this.price === 0;
}

module.exports = (sequelize: Sequelize, DataTypes: Sequelize.DataTypes) => {
  const themesScheme = sequelize.define('theme', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [2, 200]
      }
    },
    version: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    imageUrl: {
      type: DataTypes.STRING,
      allowNull: true
    },
    source: {
      type: DataTypes.STRING,
      allowNull: false
    },
    scheme: {
      type: DataTypes.STRING,
      allowNull: false
    },
    price: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
      validate: {
        isInt: {
          msg: 'Wrong price number'
        }
      }
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
  }, {
    hooks: { beforeCreate },
    getterMethods: { isFree }
  });

  themesScheme.associate = function associate(db) {
    this.belongsTo(db.category);
    this.belongsTo(db.user, {
      foreignKey: {
        allowNull: false,
        fieldName: 'developerId'
      }
    });
    this.hasMany(db.template, {
      foreignKey: {
        allowNull: false
      },
    });
  };

  return themesScheme;
};
