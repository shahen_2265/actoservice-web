/**
 * @flow
 */

import Router from 'koa-router';
import createLog from 'utils/logs';
import { developerOnly } from 'utils/jwt-auth';

import {
  getThemes,
  getTheme,
  createTheme,
  uploadResource
} from './themes.handlers';

const log = createLog('themes:routes');

export function attach(app: any, prefix: string) {
  log('Initialize Themes routes');

  const router = new Router({
    prefix: `${prefix}/themes`
  });

  router.get('/', getThemes);
  router.get('/:id', getTheme);

  router.post('/', developerOnly(), createTheme);
  router.post('/resource', developerOnly({ userRequired: true }), uploadResource);

  app.use(router.routes());
  app.use(router.allowedMethods());
}
