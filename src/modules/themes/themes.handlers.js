/**
 * @flow
 */

import { Op } from 'sequelize';
import multiparty from 'multiparty';
import omit from 'lodash/omit';
import get from 'lodash/get';
import { s3Util } from 'utils/resources';

// $FlowFixMe
import { uploadResource as uploadService } from 'services/resources';

type ThemeDraft = {|
  name?: string,
  description?: string,
  version?: number,
  source: string,
  scheme: string,
  imageUrl?: string
|};

export async function getThemes(ctx: any) {
  const { prev, limit } = ctx.query;

  const query = {
    attributes: {
      exclude: ['deleted']
    },
    where: {
      deleted: false
    },
    limit
  };

  if (prev) {
    query.where.id = {
      [Op.gt]: prev
    };
  }
  ctx.body = await ctx.models.theme.findAll(query);
}

export async function getTheme(ctx: any) {
  ctx.body = 1;
}

export async function createTheme(ctx: any) {
  const themeForm = new multiparty.Form();
  const newTheme: ThemeDraft = {
    source: '',
    scheme: '',
  };

  const { theme } = ctx.models;

  const createPromise = new Promise((resolve, reject) => {
    themeForm.parse(ctx.req, async (err, fields, files) => {

      newTheme.name = fields.name.join('');

      newTheme.version = parseInt(fields.version[0], 10);
      newTheme.description = fields.description.join('');

      newTheme.developerId = ctx.state.developer.id;

      const excludeKeys = Object.keys(omit(files, ['configMap', 'source', 'image'])) || [];

      const assets = excludeKeys
        .map(fileKey => ({
          file: files[fileKey][0].path,
          key: files[fileKey][0].originalFilename
        }));
      // Upload to S3
      const s3Object = {
        key: s3Util.themePath(newTheme.name).folder,
        source: {
          file: files.source[0].path
        },
        configMap: {
          file: files.configMap[0].path
        },
        image: {
          file: get(files, 'image[0].path')
        },
        assets
      };

      theme
        .create(newTheme, { files: s3Object })
        .then(resolve)
        .catch(reject);
    });
  });

  return createPromise
    .then((themeRecord) => {
      ctx.body = themeRecord;
    })
    .catch(e => ctx.throw(400, e));
}


export async function uploadResource(ctx: any) {
  return uploadService(ctx, { isTheme: true })
    .then(res => (ctx.body = res));
}
