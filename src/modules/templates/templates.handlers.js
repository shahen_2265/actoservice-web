import isEmpty from 'lodash/isEmpty';
import isNil from 'lodash/isNil';
import get from 'lodash/get';

import s3 from 'utils/s3';

import mixTemplateScheme from 'utils/kernel/mixTemplateScheme';
import mergeWithUpdates from 'utils/kernel/mergeWithUpdates';
import isCompatableUpdates from 'utils/kernel/isCompatableUpdates';
import isTemplateCompatableWithTheme from 'utils/kernel/isTypeCompatableWithTheme';

export async function createTemplate(ctx) {

}

export async function updateTemplate(ctx) {
  const { organizations } = ctx.state;
  const { body: payload } = ctx.request;

  if (isEmpty(payload)) {
    ctx.throw(400);
  }
  const requestedOrganizationId = get(payload, 'organizationId');
  const updates = get(payload, 'updates');

  if (isEmpty(updates)) {
    ctx.throw(400, 'Updates are required');
  }
  if (isNil(requestedOrganizationId)) {
    ctx.throw(400, 'OrganizationId is required');
  }
  if (isEmpty(organizations)) {
    ctx.throw(401);
  }
  const isOwner = organizations.find(org =>
    +org.id === +requestedOrganizationId);

  if (!isOwner) {
    ctx.throw(401, 'xxCCCxx');
  }
  const [template, organization] = await Promise.all([
    s3.getObject(`templates/o/${requestedOrganizationId}/template.json`),
    ctx.models.organization.findOne({
      attributes: ['path'],
      where: {
        id: requestedOrganizationId
      }
    })
  ]);
  const { data, err } = template;

  if (err) {
    ctx.throw(422, err);
  }

  if (!isCompatableUpdates(data, updates)) {
    ctx.throw(400, 'Incompatable updates');
  }
  const theme = await ctx.cache.getOrganizationTheme(organization.path);
  if (!theme) {
    ctx.throw(403, 'This shouldnt be');
  }

  const {
    data: themeData,
    err: themeError
  } = await s3.getObject(`themes/${theme}/scheme.json`);

  if (themeError) {
    ctx.throw(422, err);
  }

  const updatedTemplate = mergeWithUpdates(data, updates);
  if (!isTemplateCompatableWithTheme(updatedTemplate, themeData)) {
    ctx.throw(400, 'Updates are incompatable with theme');
  }
  try {
    await s3.uploadTemplate({
      key: requestedOrganizationId,
      scheme: JSON.stringify(updatedTemplate)
    });

    await ctx.cache.setThemeMixedTemplate(
      organization.path,
      mixTemplateScheme(updatedTemplate, themeData)
    );

    ctx.body = updatedTemplate;
  } catch (e) {
    ctx.throw(422, e);
  }
}

export async function getTemplate(ctx) {
  const { organization } = ctx.query;
  if (isEmpty(organization)) {
    ctx.throw(400);
  }
  const { organizations } = ctx.state;
  if (isEmpty(organizations)) {
    ctx.throw(404);
  }
  const isOwner = organizations
    .some(org => org.path === organization);

  if (!isOwner) {
    ctx.throw(403);
  }
  const org = await ctx.models.organization.findOne({
    attributes: [],
    where: { path: organization },
    include: [{
      attributes: ['scheme'],
      model: ctx.models.template,
      required: true
    }]
  });
  if (isEmpty(org)) {
    ctx.throw(404);
  }
  const { data, err } = await s3.getObject(org.template.scheme, { disablePrefix: true });

  if (err) {
    ctx.throw(422, err);
  }
  ctx.body = data;
}
