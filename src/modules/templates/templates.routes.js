import Router from 'koa-router';
import Boom from 'boom';
import createLog from 'utils/logs';
import { authRequired } from 'utils/jwt-auth';
import {
  createTemplate,
  getTemplate,
  updateTemplate
} from './templates.handlers';

const log = createLog('templates:routes');

export function attach(app, prefix) {
  const templatesPrefix = `${prefix}/templates`;

  const router = new Router({
    prefix: templatesPrefix
  });

  router.get('/', authRequired, getTemplate);
  router.post('/', authRequired, createTemplate);
  router.put('/', authRequired, updateTemplate);

  app.use(router.routes());

  app.use(router.allowedMethods({
    notImplemented: () => new Boom.notImplemented(),
    methodNotAllowed: () => new Boom.methodNotAllowed()
  }));

  log('templates routes initialized');
}
