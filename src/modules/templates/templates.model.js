/**
 * @flow
 */
import createLog from 'utils/logs';
import s3 from 'utils/s3';
import isNil from 'lodash/isNil';
import Sequelize from 'sequelize';

const log = createLog('template:model');

log('Initialize Templates model');

type Data = {
  [key: string]: Data
};

type CreatePayload = {
  orgId: number,
  scheme: string
};

async function beforeCreate(data: CreatePayload, { organizationId }) {
  if (isNil(organizationId)) {
    throw new Error('organiztion should be specified before creating template');
  }
  const { scheme } = data;
  try {
    const result = await s3.uploadTemplate({ key: organizationId, scheme });
    // eslint-disable-next-line
    data.scheme = result.Location;
  } catch (e) {
    throw new Error(`Failed to upload template: ${e.stack}`);
  }
}

module.exports = (sequelize: Sequelize, DataTypes: Sequelize.DataTypes) => {
  const templatesScheme = sequelize.define('template', {
    scheme: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    hooks: { beforeCreate }
  });

  templatesScheme.associate = function (db) {
    this.hasOne(db.organization);
    this.belongsTo(db.theme);
  };

  return templatesScheme;
};
