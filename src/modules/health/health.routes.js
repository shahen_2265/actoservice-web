import Router from 'koa-router';
import Boom from 'boom';
import createLog from 'utils/logs';
import { checkHealth } from './health.handlers';

const log = createLog('health:routes');

export function attach(app, prefix) {
  const organizationsPrefix = `${prefix}/health`;

  const router = new Router({
    prefix: organizationsPrefix
  });

  router.get('/', checkHealth);
  app.use(router.routes());

  app.use(router.allowedMethods({
    notImplemented: () => new Boom.notImplemented(),
    methodNotAllowed: () => new Boom.methodNotAllowed()
  }));

  log('health routes initialized');
}
