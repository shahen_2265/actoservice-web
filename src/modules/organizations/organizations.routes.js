import Router from 'koa-router';
import Boom from 'boom';
import createLog from 'utils/logs';
import { authRequired } from 'utils/jwt-auth';
import {
  createOrganization,
  getOrganization,
  getUserOrganizations,
  getOrganizationTheme
} from './organizations.handlers';


const log = createLog('organization:routes');

export function attach(app, prefix) {
  const organizationsPrefix = `${prefix}/organizations`;

  const router = new Router({
    prefix: organizationsPrefix
  });

  router.get('/', async (ctx, next) => {
    if (ctx.query.subdomain) {
      return getOrganization(ctx);
    }
    await authRequired(ctx, next);
    return getUserOrganizations(ctx);
  });
  router.post('/', authRequired, createOrganization);
  router.get('/:subdomain/theme', authRequired, getOrganizationTheme);

  app.use(router.routes());

  app.use(router.allowedMethods({
    notImplemented: () => new Boom.notImplemented(),
    methodNotAllowed: () => new Boom.methodNotAllowed()
  }));

  log('organization routes initialized');
}
