/**
 * @flow
 */

import isNil from 'lodash/isNil';
import trim from 'lodash/trim';
import get from 'lodash/get';
import map from 'lodash/map';
import omit from 'lodash/omit';
import isEmpty from 'lodash/isEmpty';
import processData from 'utils/processData';
import s3 from 'utils/s3';
import { s3Util } from 'utils/resources';
import generateTemplate from 'utils/kernel/generateTemplate';

import config from 'config';
/**
 * We should send OK if organization is free (subdomain is free)
 * If this subdomain is not a valid we should send 400/422 status
 */
export async function getOrganization(ctx: any) {
  const { organization } = ctx.models;

  const { subdomain } = ctx.query;

  /**
   * If this request comes not from www.actoservice.com
   * Or subdomain wasn't send throw error
   */
  if (!ctx.state.isActoservice || isEmpty(trim(subdomain))) {
    ctx.throw(400);
  }
  try {
    const potentialOrg = await organization.findOne({
      where: { path: subdomain }
    });

    ctx.status = isNil(potentialOrg)
      ? 200
      : 422;
  } catch (e) {
    ctx.throw(400, e);
  }

  if (config.organizations.subdomainInvalidator.test(subdomain)) {
    ctx.throw(422);
  }
}

export async function getUserOrganizations(ctx: any) {
  const mOrganizations = await ctx.models.organization.findAll({
    include: [{
      required: true,
      model: ctx.models.template,
      attributes: ['id'],
      include: [{
        required: true,
        attributes: ['imageUrl', 'name'],
        model: ctx.models.theme
      }]
    }],
    where: {
      userId: ctx.state.user.id
    }
  });
  const organizations = map(mOrganizations, (org) => {
    const orgJson = org.toJSON();
    return {
      ...omit(orgJson, ['template']),
      theme: get(orgJson, 'template.theme')
    };
  });
  ctx.body = organizations;
  if (isEmpty(organizations)) {
    ctx.status = 200;
    ctx.body = [];
  }
}

export async function createOrganization(ctx: any) {
  const runTransaction = ctx.db.transaction.bind(ctx.db);

  const {
    organization: organizationModel,
    theme: themeModel
  } = ctx.models;

  // $FlowFixMe
  const newOrg = processData(ctx.request.body, {
    lowerCase: ['email', 'path']
  });

  // $FlowFixMe
  newOrg.userId = ctx.state.user.id;

  if (isNil(newOrg)) {
    ctx.throw(400, 'Organization info is not complete');
  }

  const creation = new Promise((resolve, reject) => {
    let t = null;
    runTransaction()
      .then(async (transaction) => {
        t = transaction;

        // Create Organization
        const newOrganization = await organizationModel.create(newOrg, {
          transaction
        });

        // Find Default Theme
        const baseTheme = await themeModel.findOne({
          where: { id: 1 }
        });

        if (isNil(baseTheme)) {
          t.rollback();
          const err = new Error('Base theme not found in storage');
          reject(err);
          return;
        }

        // Get Theme's Scheme.json from S3 $FlowFixMe
        const { data, err } = await s3.getObject(baseTheme.scheme, { disablePrefix: true });
        if (err) {
          t.rollback();
          ctx.throw(400, 'Base theme is invalid');
        }
        // Generate blank template based on the Theme
        const blankTemplate = generateTemplate(data);

        // Create Template
        await newOrganization.createTemplate({
          scheme: JSON.stringify(blankTemplate),
          themeId: baseTheme.id
        }, { organizationId: newOrganization.id, transaction });

        // ctx.setTheme(baseTheme.name);

        // Save in the cache organization theme
        await ctx.cache.setOrganizationTheme(
          newOrganization.path,
          baseTheme.name
        );

        t.commit();
        resolve({ organization: newOrganization });
      })
      .catch((e) => {
        if (t) {
          t.rollback();
        }
        reject(e);
      });
  });

  return creation
    .then(({ organization }: { organization: * }) => {
      const organizations = ctx.state.organizations || [];
      organizations.push(organization);
      ctx.generateToken(ctx.state.user, organizations);
      ctx.status = 201;
      ctx.body = organization;
    })
    .catch(e => ctx.throw(422, e));
}

export async function getOrganizationTheme(ctx: any) {
  const { object } = ctx.query;
  const { subdomain } = ctx.params;

  const { organizations } = ctx.state;

  if (isEmpty(trim(subdomain))) {
    ctx.throw(400);
  }
  if (isEmpty(organizations)) {
    ctx.throw(401);
  }
  const orgByPath = organizations.find(org => org.path === subdomain);

  if (isNil(orgByPath)) {
    ctx.throw(403);
  }

  const theme = await ctx.cache.getOrganizationTheme(orgByPath.path);
  if (isNil(theme)) {
    ctx.throw(404, 'Theme not found');
  }
  if (isEmpty(trim(object))) {
    ctx.throw(400, 'File name is required');
  }
  const path = `${s3Util.themePath(theme).folder}/${trim(object)}`;

  const { data, err } = await s3.getObject(path);

  if (err) {
    ctx.throw(400, `Can't find object - ${object}`);
  }
  ctx.body = data;
}
