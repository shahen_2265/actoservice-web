/**
 * @flow
 */
import createLog from 'utils/logs';
import config from 'config';
import moment from 'moment';
import Sequelize from 'sequelize';

const log = createLog('organizations:model');

log('Initialize Organization model');

function beforeCreate(data) {
  if (data.expiresAt) {
    // eslint-disable-next-line
    data.expiresAt = moment(data.expiresAt).format(config.organizations.expireDateFormat);
  }
  return data;
}

module.exports = (sequelize: Sequelize, DataTypes: Sequelize.DataTypes) => {
  const organizationsSchema = sequelize.define('organization', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [2, 200]
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    expiresAt: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      defaultValue: () => moment().format(config.organizations.expireDateFormat)
    },
    path: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notIn: [config.organizations.blacklistedSubdomains],
        isPathValid(value: string) {
          if (config.organizations.subdomainInvalidator.test(value)) {
            throw new Error('Path shouldn\'t contain special characters');
          }
        }
      }
    },
    isActive: {
      type: DataTypes.VIRTUAL,
      get() {
        return moment(this.expiresAt).isAfter(moment.now());
      }
    },
    isExpired: {
      type: DataTypes.VIRTUAL,
      get() {
        return moment(this.expiresAt).isBefore(moment.now());
      }
    },
    todayExpires: {
      type: DataTypes.VIRTUAL,
      get() {
        return moment(this.expiresAt).isSame(moment.now());
      }
    },
  }, {
    indexes: [
      { unique: true, fields: ['path'] }
    ],
    hooks: { beforeCreate },
  });

  organizationsSchema.associate = function associate(db) {
    this.belongsTo(db.user, {
      onDelete: 'CASCADE',
      foreignKey: {
        allowNull: false
      }
    });
    this.belongsTo(db.template, {
      onDelete: 'CASCADE'
    });
  };

  organizationsSchema.prototype.getOwner = function getOwner() {
    return this.getUser({
      where: { isOwner: true }
    });
  };

  return organizationsSchema;
};
