/**
 * @flow
 */

import omit from 'lodash/omit';
import isNil from 'lodash/isNil';
import processData from 'utils/processData';

export async function signinUser(ctx: any) {
  const { user } = ctx.models;
  const potentialUser = await user.findOne({
    where: {
      email: ctx.request.body.email
    }
  });
  if (isNil(potentialUser)) {
    ctx.throw(404);
  }
  const { isValid } = await potentialUser.comparePassword(ctx.request.body.password);
  if (!isValid) {
    ctx.throw(404);
  }
  const userOrgs = await potentialUser.getOrganizations();
  ctx.generateToken(omit(potentialUser.toJSON(), ['password', 'apiKey']), userOrgs);

  potentialUser.organizations = userOrgs;

  ctx.body = {
    ...omit(potentialUser.toJSON(), ['password']),
    organizations: userOrgs
  };
  ctx.status = 200;
}

export async function signupUser(ctx: any) {
  const {
    name,
    phone,
    email,
    password,
  } = ctx.request.body;

  const user = processData({
    name,
    phone,
    email,
    password,
  }, {
    lowerCase: ['email']
  });

  try {
    const newUser = await ctx.models.user.create(user);

    ctx.body = omit(newUser.toJSON(), ['password']);
    ctx.status = 201;
    ctx.generateToken(ctx.body);
  } catch (e) {
    ctx.throw(422, e);
  }
}

export async function getCurrentUser(ctx: any) {
  const { id } = ctx.state.user;
  try {
    const user = await ctx.model.user.findOne({
      where: { id }
    });
    if (isNil(user)) {
      ctx.throw(404);
    }
    ctx.body = user.toJSON();
  } catch (e) {
    ctx.throw(422, e);
  }
}
