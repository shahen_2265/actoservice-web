/**
 * @flow
 */

import Router from 'koa-router';
import Boom from 'boom';
import createLog from 'utils/logs';
import config from 'config';
import { authRequired } from 'utils/jwt-auth';

import {
  signinUser,
  signupUser,
  getCurrentUser
} from './users.handlers';

const log = createLog('user:routes');

export function attach(app: any, prefix: string) {
  const userPrefix = `${prefix}/users`;
  const authPrefix = `${prefix}/auth`;

  const userRouter = new Router({
    prefix: userPrefix
  });
  const authRouter = new Router({
    prefix: authPrefix
  });

  authRouter.post('/signin', signinUser);
  authRouter.post('/signup', signupUser);

  userRouter.get('/me', authRequired, getCurrentUser);

  if (!config.isProd) {
    userRouter.get('/', (ctx) => {
      console.log(ctx.headers);
      ctx.models.user.destroy({
        where: {
          id: ctx.state.user.id
        }
      });
    });
  }
  app.use(userRouter.routes());
  app.use(authRouter.routes());

  app.use(userRouter.allowedMethods({
    notImplemented: () => new Boom.notImplemented(),
    methodNotAllowed: () => new Boom.methodNotAllowed()
  }));

  app.use(authRouter.allowedMethods({
    notImplemented: () => new Boom.notImplemented(),
    methodNotAllowed: () => new Boom.methodNotAllowed()
  }));

  log('User and Auth routes initialized');
}
