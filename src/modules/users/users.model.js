/**
 * @flow
 */

import bcrypt from 'bcrypt';
import createLog from 'utils/logs';
import { validateApiKeyBasic } from 'utils/apiKey';
// $FlowFixMe
import ApiKeyError from 'errors/ApiKeyError';
import Sequelize from 'sequelize';

const log = createLog('users:model');
const SALT_WORK_FACTOR = 10;

log('Initialize User model');

module.exports = (sequelize: Sequelize, DataTypes: Sequelize.DataTypes) => {
  function beforeCreate(user) {
    return bcrypt.genSalt(SALT_WORK_FACTOR)
      .then(salt => bcrypt.hash(user.password, salt))
      .then((password) => {
        // eslint-disable-next-line
        user.password = password;
        return user;
      })
      .catch((e) => {
        log('error on password hashing ', user);
        throw e;
      });
  }

  const usersSchema = sequelize.define('user', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notIn: [['admin']],
        len: [2, 200]
      }
    },
    phone: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },
    apiKey: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        isValid(value) {
          const { err } = validateApiKeyBasic(value);
          if (err) {
            throw new ApiKeyError('Api Key is not valid');
          }
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [4, 50]
      }
    },
  }, {
    indexes: [
      { unique: true, fields: ['phone'] },
      { unique: true, fields: ['email'] }
    ],
    hooks: { beforeCreate },
  });

  usersSchema.associate = function associate(db) {
    this.hasMany(db.organization, {
      foreignKey: { allowNull: false },
      onDelete: 'CASCADE'
    });
  };

  usersSchema.prototype.comparePassword = function comparePassword(password: string) {
    return bcrypt.compare(password, this.password)
      .then(isValid => ({ isValid }))
      .catch(err => ({ err, isValid: false }));
  };

  return usersSchema;
};
