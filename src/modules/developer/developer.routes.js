import Router from 'koa-router';
import Boom from 'boom';
import createLog from 'utils/logs';
import { authRequired } from 'utils/jwt-auth';
import { getApiKey } from './developer.handlers';

const log = createLog('developer:routes');

export function attach(app, prefix) {
  const organizationsPrefix = `${prefix}/developer`;

  const router = new Router({
    prefix: organizationsPrefix
  });

  router.get('/key', authRequired, getApiKey);

  app.use(router.routes());

  app.use(router.allowedMethods({
    notImplemented: () => new Boom.notImplemented(),
    methodNotAllowed: () => new Boom.methodNotAllowed()
  }));

  log('developers routes initialized');
}
