/**
 * @flow
 */
import isEmpty from 'lodash/isEmpty';

export async function getApiKey(ctx: any) {
  const { user } = ctx.state;
  if (isEmpty(user)) {
    ctx.throw(403);
  }
  const key = await ctx.generateApiKey();
  ctx.body = { apiKey: key };
}
