/**
 * @flow
 */
import jwt from 'jsonwebtoken';

// const themeKey = 'X-Theme';

const cookieName = process.env.JWT_COOKIE;
const signature = process.env.TOKEN_SIGNATURE || '';
const secret = process.env.JWT_SECRET;

// const genThemeHeaderForId = (id: number) =>
//   `${themeKey}-${id}`;

export default async function (ctx: any, next: Function) {
  ctx.setToken = (token: string) => ctx.set(cookieName, token);
  ctx.getToken = () => ctx.get(cookieName);

  // ctx.getTheme = (id: number) => ctx.get(genThemeHeaderForId(id));
  // ctx.setTheme = (id: number, themeName: string) => ctx.set(genThemeHeaderForId(id), themeName);

  ctx.removeToken = () => ctx.set(cookieName, null);
  ctx.generateToken = (user, organizations) => {
    const token = jwt.sign({ user, organizations }, process.env.JWT_SECRET, {
      expiresIn: '100d'
    });
    ctx.setToken(`${signature} ${token}`);
  };

  ctx.isTokenValid = (token: string) => {
    try {
      const signedToken = token.split(`${signature} `)[1];
      const { user } = jwt.verify(signedToken, secret);
      return user;
    } catch (e) {
      return false;
    }
  };

  await next();
}
