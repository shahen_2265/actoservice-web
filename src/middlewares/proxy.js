/**
 * @flow
 */

import proxy from 'koa-proxy';
import convert from 'koa-convert';
import config from 'config';
import { URL } from 'url';
import serve from 'koa-static';
import { s3Util } from 'utils/resources';
import { getPreviewTheme, isPreviewPage } from 'services/preview';

const indexPaths = [
  '',
  '/',
  'index.html'
];

const { actoserviceFolderPath } = config.web;

export default async function (ctx: any, next: Function) {
  // If request is not GET, or request sent to /api - Pass middleware
  if (ctx.method.toLowerCase() !== 'get' || /^\/api/gi.test(ctx.path)) {
    await next();
    return null;
  }

  const isPreview = isPreviewPage(ctx);
  const previewTheme = getPreviewTheme(ctx);

  console.log(
    '\n\n\n',
    'ctx.path => ' + ctx.path + '\n',
    'isPreview => ' + isPreview + '\n',
    'previewTheme => ' + previewTheme + '\n',
    '\n\n\n'
  );
  if (isPreview && previewTheme) {
    const reqFile = ctx.path.replace('/_', '');

    const fileUrl = `${s3Util.themePath(previewTheme, true).folder}${reqFile}`;
    console.log('File Url to serve', fileUrl);

    return convert(proxy({
      // $FlowFixMe
      host: config.s3.host.replace(process.env.S3_BUCKET, ''),
      url: fileUrl
    }))(ctx, next);
  }

  /**
   * If req sent from subdomain and path is not a index
   * Proxy to S3 bucket themes folder corresponding
   * for given organization theme
   */
  if (ctx.state.organizationSubdomain && !indexPaths.includes(ctx.path)) {
    const theme = await ctx.cache.getOrganizationTheme(ctx.state.organizationSubdomain);

    return convert(proxy({
      // $FlowFixMe
      host: config.s3.host.replace(process.env.S3_BUCKET, ''),
      url: `${s3Util.themePath(theme, true).folder}${ctx.path}`
    }))(ctx, next);
  }

  /**
   * if it's a index path and request sent from www.actoservice.com
   * Serve files
   */
  if (!indexPaths.includes(ctx.path) && !ctx.state.organizationSubdomain) {
    return serve(actoserviceFolderPath, {
      gzip: true
    })(ctx, next);
  }
  /**
   * If no one case matches just pass
   */
  await next();
  return null;
}
