/**
 * @flow
*/
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';
import pick from 'lodash/pick';
import {
  generateApiKey,
  validateApiKeyBasic,
  parseAPIKey
} from 'utils/apiKey';

const devApiKey = process.env.API_KEY_HEADER;

export default async function attachDeveloperFn(ctx: any, next: Function) {
  ctx.getApiKey = () => ctx.get(devApiKey);

  ctx.state.developer = parseAPIKey(ctx.getApiKey()).payload;

  ctx.validateApiKey = async ({ throwError, userRequired } = {}) => {
    const { user } = ctx.state;

    if (isEmpty(user) && userRequired) {
      if (throwError) {
        ctx.throw(403, 'User not found');
      }
      return false;
    }
    const apiKey = ctx.getApiKey();
    const { err } = validateApiKeyBasic(apiKey);

    if (err) {
      if (throwError) {
        ctx.throw(403, err);
      }
      return false;
    }

    if (!userRequired) {
      const existance = await ctx.models.user.findOne({
        attributes: ['id'],
        where: { apiKey }
      });

      return !isEmpty(existance);
    }

    const correctApiKey = await ctx.cache.getDeveloperApiKey(ctx.state.user.id);

    if (correctApiKey) {
      return correctApiKey === apiKey;
    }

    const requestedUser = await ctx.models.user.findOne({
      where: {
        id: ctx.state.user.id,
        apiKey
      }
    });

    if (isEmpty(requestedUser)) {
      if (throwError) {
        ctx.throw(403);
      }
      return false;
    }

    return true;
  };

  ctx.generateApiKey = async () => {
    if (isEmpty(ctx.state.user)) {
      ctx.throw(403, 'user not found');
    }

    /**
     * Check Cache
     */
    const cacheApiKey = await ctx.cache.getDeveloperApiKey(ctx.state.user.id);
    if (cacheApiKey) {
      return cacheApiKey;
    }

    /**
     * If Cache is empty check database
     */
    const databaseApiKey = await ctx.models.user.findOne({
      where: { id: ctx.state.user.id },
      attributes: ['apiKey']
    });

    if (get(databaseApiKey, 'apiKey')) {
      /**
       * If key exists in database then update cache
       */
      ctx.cache.setDeveloperApiKey(ctx.state.user.id, get(databaseApiKey, 'apiKey'));
      return get(databaseApiKey, 'apiKey');
    }

    /**
     * If this is first time
     * Generate API_KEY
     * Update user model
     * Update Cache
     */
    const newApiKey = generateApiKey(pick(ctx.state.user, ['id', 'email', 'phone']));
    await ctx.models.user.update({
      apiKey: newApiKey
    }, { where: { id: ctx.state.user.id } });

    ctx.cache.setDeveloperApiKey(ctx.state.user.id, newApiKey);

    return newApiKey;
  };

  await next();
}
