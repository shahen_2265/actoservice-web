import toLower from 'lodash/toLower';

export default async function (ctx, next) {
  const organization = ctx.subdomains[0];

  if (toLower(organization) === 'www') {
    ctx.state.isActoservice = true;
    await next();
    return;
  }

  if (process.env.BUILD && !organization) {
    ctx.state.isActoservice = true;
    await next();
    return;
  }

  if (!organization) {
    await next();
    return;
  }

  ctx.state.organizationSubdomain = toLower(organization);
  await next();
}
