// @flow

import { database } from 'lib/database';

export default async function (ctx: any, next: Function) {
  ctx.db = database.sequelize;
  ctx.models = database.models;

  await next();
}
