/**
 * @flow
 */
import pick from 'lodash/pick';

const defaultValues = {
  limit: 20,
  prev: null
};

export default async function querySanitize(ctx: any, next: Function) {
  const values = pick(ctx.query, Object.keys(defaultValues));

  Object.keys(values)
    .forEach(key => ctx.query[key] = Number(values[key]) || defaultValues[key]);

  await next();
}
