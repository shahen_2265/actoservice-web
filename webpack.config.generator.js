const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyWebpackPlugin = require('uglifyjs-webpack-plugin');

const cssExtractor = new ExtractTextPlugin({
  filename: '_def_styles.css',
  allChunks: true
});
const scssExtractor = new ExtractTextPlugin({
  filename: 'bundle.css',
  allChunks: true
});
const bootstrapExtractor = new ExtractTextPlugin('bootstrap.css');

const modeByFlag = {
  true: 'production',
  false: 'development'
};

const devCssIdent = '[local]---[path][name]---[hash:base64:5]';
const prodCssIdent = '[hash:base64]';

const BUILD_ENV = {
  CLIENT: 'client',
  SERVER: 'server',
  ACTOSERVICE: 'actoservice'
};

module.exports = function generator(buildEnv, isProd) {
  const isServer = buildEnv === BUILD_ENV.SERVER;

  const buildDirector = buildEnv === BUILD_ENV.ACTOSERVICE
    ? 'actoservice-build'
    : 'public';

  const babelEnv = buildEnv;

  const assetsPath = isServer
    ? path.join(__dirname, 'server')
    : path.join(__dirname, buildDirector);

  const jsLoader = {
    test: /\.js$|\.jsx$/,
    use: {
      loader: 'babel-loader',
      options: {
        forceEnv: babelEnv,
        cacheIdentifier: babelEnv,
        cacheDirectory: false,
        plugins: ['react-hot-loader/babel'],
      }
    },
    exclude: /node_modules/
  };

  const urlLoader = {
    test: /\.(png|jpg|gif|svg|woff|eot|ttf|woff2)$/,
    use: [{
      loader: 'file-loader',
      options: {
        limit: 8192
      }
    }]
  };

  const bootstrapLoader = {
    test: /\.scss$/,
    include: [
      path.resolve(__dirname, 'node_modules/bootstrap'),
      path.resolve(__dirname, 'app/actoservice/src/styles/overrides.scss')
    ],
    use: isProd
      ? bootstrapExtractor.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader',
        }, {
          loader: 'sass-loader'
        }]
      })
      : [{
        loader: 'style-loader'
      }, {
        loader: 'css-loader',
      }, {
        loader: 'sass-loader'
      }]
  };

  const _local = [{
    loader: 'css-loader',
    options: {
      modules: true,
      localIdentName: isProd ? prodCssIdent : devCssIdent
    },
  }, {
    loader: 'sass-loader'
  }];

  const localStylesLoader = {
    test: /\.scss$/,
    include: path.resolve(__dirname, 'app/actoservice'),
    exclude: path.resolve(__dirname, 'app/actoservice/src/styles/overrides.scss'),
    use: isProd
      ? scssExtractor.extract({ use: _local, fallback: 'style-loader' })
      : [{ loader: 'style-loader' }, ..._local],
  };

  const _css = ['style-loader', 'css-loader'];
  const cssLoader = {
    test: /\.css$/,
    use: isProd
      ? cssExtractor.extract({ fallback: _css[0], use: [_css[1]] })
      : _css
  };

  const configuration = {
    name: `${buildEnv}-side ${modeByFlag[isProd]} rendering`,
    entry: {
      [buildEnv]: path.join(
        __dirname,
        isServer ? 'src' : `app/${buildEnv}`,
        'index.js'
      )
    },
    devtool: 'source-map',
    output: {
      path: assetsPath,
      filename: isServer
        ? 'server.js'
        : 'bundle.js',

      publicPath: '/',
    },
    mode: modeByFlag[isProd],
    module: {
      rules: [
        jsLoader,
        localStylesLoader,
        urlLoader,
        cssLoader,
        bootstrapLoader
      ]
    },
    devServer: {
      contentBase: path.join(__dirname, buildDirector)
    },
    plugins: [
      new CleanWebpackPlugin(assetsPath),
      new webpack.DefinePlugin({
        __DEV__: !isProd,
        __SERVER__: isServer,
        'process.env.NODE_ENV': JSON.stringify(modeByFlag[isProd])
      }),
      new webpack.NamedModulesPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      bootstrapExtractor,
      cssExtractor,
      scssExtractor
    ],
  };

  if (!isServer) {
    configuration.plugins.push(new CopyWebpackPlugin([{
      from: 'app/actoservice/favicon.ico',
      to: 'favicon.ico'
    }]));
  }


  configuration.plugins.push(new HtmlWebpackPlugin({
    template: path.join(
      __dirname,
      buildEnv === BUILD_ENV.ACTOSERVICE
        ? 'app/actoservice'
        : 'app/client',

      'index.html'
    ),
    inject: 'body'
  }));

  if (isProd) {
    configuration.optimization = {
      minimizer: [
        new UglifyWebpackPlugin({
          parallel: true,
          sourceMap: false,
        })
      ]
    };
  }
  return configuration;
};


module.exports.BUILD_ENV = BUILD_ENV;
