// import 'babel-polyfill';

import 'bootstrap/scss/bootstrap.scss';
import 'styles/overrides.scss';


import React from 'react';
import { render } from 'react-dom';
import { TranslationsProvider } from 'components/Translations';
import Provider from 'modules/Provider';
import App from './src/app';

render(
  <Provider>
    <TranslationsProvider>
      <App />
    </TranslationsProvider>
  </Provider>,
  document.getElementById('root')
);
