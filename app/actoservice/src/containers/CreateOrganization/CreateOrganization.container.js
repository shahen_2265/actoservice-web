/**
 * @flow
 */
import * as React from 'react';
import Helmet from 'react-helmet';

import CreateOrganizationForm from './components/CreateOrganizationForm';
import Header from './components/Header';

class CreateOrganization extends React.Component<void> {
  openDashboard = () => this.props.history.push('/dashboard');

  render() {
    return (
      <div>
        <Helmet>
          <title>Create new Organization</title>
        </Helmet>
        <Header />
        <CreateOrganizationForm onSuccess={this.openDashboard} />
      </div>
    );
  }
}

export default CreateOrganization;
