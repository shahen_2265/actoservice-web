/**
 * @flow
 */
import type { ComponentType } from 'react';
import { reduxForm, getFormValues, hasSubmitSucceeded } from 'redux-form';
import isNil from 'lodash/isNil';
import get from 'lodash/get';
import { isEmail } from 'utils/validators';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createOrganization } from 'modules/organizations';
import { SUBDOMAIN_INVALIDATOR } from 'constants';

const FORM_NAME = 'createOrganizationForm';

type Values = {
  email: ?string,
  path: ?string,
  phone: ?string,
  name: ?string
};
const validate = (values: Values) => {
  const errors: Values = {
    email: null,
    path: null,
    phone: null,
    name: null
  };

  const path = get(values, 'path');

  if (isNil(get(values, 'name'))) {
    errors.name = 'Organization name is required';
  }
  if (isNil(get(values, 'phone'))) {
    errors.phone = 'Organization phone is required';
  }
  if (isNil(get(values, 'email'))) {
    errors.email = 'Organization email is required';
  }
  if (!isEmail(get(values, 'email'))) {
    errors.email = 'Invalid email';
  }

  if (SUBDOMAIN_INVALIDATOR.test(path)) {
    errors.path = 'Subdomain should not contain special characters';
  }
  if (isNil(path)) {
    errors.path = 'Please enter your subdomain';
  }

  return errors;
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ createOrganization }, dispatch);
}

function mapStateToProps(state) {
  return {
    isSucceeded: hasSubmitSucceeded(FORM_NAME)(state)
  };
}

export default function (form: ComponentType<*>) {
  return reduxForm({
    form: FORM_NAME,
    validate,
    destroyOnUnmount: false,
  })(form);
}

export function connectRedux(form: ComponentType<*>) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(form);
}
