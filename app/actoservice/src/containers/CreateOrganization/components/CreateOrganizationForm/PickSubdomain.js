/**
 * @flow
 */
import * as React from 'react';
import { Alert, Col, Row, Container, Input } from 'reactstrap';
import { SyncLoader } from 'react-spinners';
import { COLORS } from 'constants';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Text from 'components/UI/Text';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import cx from 'classnames';
import Field from 'redux-form/lib/Field';
import styles from './styles.scss';
import connect from './connect';

const mainRowClasses = cx(styles.mainRow, 'justify-content-center');

const renderSubdomainTextField = ({
  input,
  label,
  meta: { touched, error },
  isSubdomainTaken,
  checkingSubdomain,
  ...custom
}) => (
  <React.Fragment>
    <Input
      placeholder={label}
      {...input}
      {...custom}
    />
    <Text size={Text.sizes.xs} className={styles.errorMsg}>
      {(touched && error) || (isSubdomainTaken && 'This sub-domain already taken')}
    </Text>
  </React.Fragment>
);

type Props = {
  handleSubmit: Function,
  validateSubdomain: Function,
  valid: boolean,
  submitting: boolean,
  isSubdomainTaken: boolean,
  isSubdomainValid: boolean,
  checking: boolean
};

function PickSubdomain({
  handleSubmit,
  valid,
  isSubdomainTaken,
  checking,
  submitting,
  validateSubdomain,
  isSubdomainValid
}: Props) {
  return (
    <Container fluid>
      <Alert className={styles.alert} isOpen={checking}>
        <div className={styles.snackbarMessage}>
          <SyncLoader color={COLORS.PRIMARY} />
          <span>Checking your subdomain</span>
        </div>
      </Alert>
      <Row className={'justify-content-center'}>
        <Col sm={7} xs={12} className={styles.pickSubdomain}>
          <div className={styles.pickSubdomain}>
            <Paper>
              <Container className={'align-items-center'}>
                <Row className={mainRowClasses}>
                  <Col xs={6} className={'pr-0'}>
                    <Field
                      name={'path'}
                      component={renderSubdomainTextField}
                      label={'your organization'}
                      margin={'normal'}
                      checkingSubdomain={checking}
                      isSubdomainTaken={isSubdomainTaken}
                      onChange={validateSubdomain}
                      className={cx(styles.textField, styles.subdomainInput)}
                    />
                  </Col>
                  <Col xs={6} className={'d-flex align-items-center pl-0'}>
                    <Text size={Text.sizes.lg}>
                  .actoservice.com
                    </Text>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Button
                      color={'primary'}
                      onClick={handleSubmit}
                      disabled={!valid || !isSubdomainValid}
                    >
                      {submitting ? (
                        <CircularProgress />
                      ) : 'Create your website!'}
                    </Button>
                  </Col>
                </Row>
              </Container>
            </Paper>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default connect(PickSubdomain);
