/**
 * @flow
 */
import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Field from 'redux-form/lib/Field';
import renderTextField from './renderTextField';
import connect from './connect';
import styles from './styles.scss';

function OrganizationDetails({ handleSubmit }) {
  return (
    <Paper elevation={4} className={styles.paper}>
      <Field
        name={'name'}
        component={renderTextField}
        label={'Organization name'}
        margin={'normal'}
        fullWidth
        className={styles.textField}
      />
      <Field
        name={'description'}
        component={renderTextField}
        label={'Description'}
        margin={'normal'}
        multiline
        fullWidth
        className={styles.textField}
      />
      <Field
        name={'phone'}
        component={renderTextField}
        label={'Phone'}
        type={'tel'}
        margin={'normal'}
        fullWidth
        className={styles.textField}
      />
      <Field
        name={'email'}
        component={renderTextField}
        label={'Email'}
        margin={'normal'}
        fullWidth
        type={'email'}
        className={styles.textField}
      />
      <Button color={'primary'} onClick={handleSubmit}>Next Step</Button>
    </Paper>
  );
}

export default connect(OrganizationDetails);
