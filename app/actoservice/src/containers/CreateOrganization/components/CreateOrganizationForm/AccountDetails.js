/**
 * @flow
 */
import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Field from 'redux-form/lib/Field';
import styles from './styles.scss';
import renderTextField from './renderTextField';
import connect from './connect';

function AccountDetails({ handleSubmit }) {
  return (
    <Paper elevation={4} className={styles.paper}>
      <Field
        name={'user.name'}
        component={renderTextField}
        label={'Your name'}
        margin={'normal'}
        fullWidth
        className={styles.textField}
      />
      <Field
        name={'user.phone'}
        component={renderTextField}
        label={'Your Phone'}
        margin={'normal'}
        fullWidth
        type={'tel'}
        className={styles.textField}
      />
      <Field
        name={'user.email'}
        component={renderTextField}
        label={'Your email'}
        margin={'normal'}
        fullWidth
        type={'email'}
        className={styles.textField}
      />
      <Field
        name={'user.password'}
        component={renderTextField}
        label={'Password'}
        margin={'normal'}
        fullWidth
        type={'password'}
        className={styles.textField}
      />
      <Field
        name={'user.replyPassword'}
        component={renderTextField}
        label={'Repeat Password'}
        margin={'normal'}
        fullWidth
        type={'password'}
        className={styles.textField}
      />
      <Button color={'primary'} onClick={handleSubmit}>Go pick sub-domain</Button>
    </Paper>
  );
}

export default connect(AccountDetails);
