/**
 * @flow
 */

import * as React from 'react';
import Stepper from 'react-stepper-horizontal';
import { Container, Row, Col } from 'reactstrap';

import trim from 'lodash/trim';
import throttle from 'lodash/throttle';

import Api from 'api';
import { COLORS } from 'constants';

import OrganizationDetails from './OrganizationDetails';
import PickSubdomain from './PickSubdomain';

import { connectRedux } from './connect';

import styles from './styles.scss';

type State = {
  step: number,
  stepsLabel: Array<string>,
  isSubdomainTaken: boolean,
  checking: boolean,
  subdomainChecked: boolean
};

type Props = {
  valid: boolean,
  submitSucceeded: boolean,
  onSubmit: Function,
  submitting: boolean,
  createOrganization: Function,
  onSuccess: Function
};

class CreateOrganizationForm extends React.Component<Props, State> {
  state = {
    step: 1,
    isSubdomainTaken: false,
    checking: false,
    subdomainChecked: false,
    stepsLabel: ['Organization Details', 'Select sub-domain'],
  };

  subdomainAsyncValidationThrottle: typeof throttle = null;

  handleNext = () =>
    this.setState({ step: this.state.step + 1 });

  getActiveStep = () =>
    this.state.step - 1;

  isCompletedStep = (step: number) =>
    step < this.getActiveStep();

  acceptOrganizationDetails = () =>
    this.nextStep();

  acceptAccountInfo = () =>
    this.nextStep();

  nextStep = () =>
    this.setState({ step: this.state.step + 1 });

  prevStep = () =>
    this.setState({ step: this.state.step - 1 });

  jumpToStep = (step: number) =>
    this.setState({ step });

  isSubdomainValid = () =>
    !this.state.isSubdomainTaken
      && this.state.subdomainChecked
      && !this.state.checking;

  asyncSubdomainValidation = (subdomain: string) => {
    const generateDirtyState = isTaken =>
      ({ checking: false, isSubdomainTaken: isTaken, subdomainChecked: true });

    setTimeout(() => {
      Api.organizations
        .checkSubdomain(subdomain)
        .then(() => this.setState(generateDirtyState(false)))
        .catch(() => this.setState(generateDirtyState(true)));
    }, 1000);
  };

  validateSubdomain = (e) => {
    const { target: { value } } = e;

    if (!trim(value)) {
      return;
    }

    if (this.state.subdomainChecked) {
      this.setState({ subdomainChecked: false });
    }

    this.setState({
      checking: true,
      isSubdomainTaken: false
    });

    if (this.subdomainAsyncValidationThrottle) {
      this.subdomainAsyncValidationThrottle.cancel();
    }

    this.subdomainAsyncValidationThrottle = throttle(this.asyncSubdomainValidation, 500, {
      leading: false,
    });

    this.subdomainAsyncValidationThrottle(value);
  };

  acceptSubdomain = (_, dispatch, form) => {
    const { values } = form;

    return new Promise((resolve, reject) => {
      this.props.createOrganization({
        organization: values,
        resolve,
        reject
      });
    })
    .then(() => this.props.onSuccess());
  }

  genSteps = () => this.state.stepsLabel.map((step, i) => ({
    title: step,
    onClick: this.jumpToStep.bind(null, i + 1)
  }));

  renderStepContent = () => {
    const { step, isSubdomainTaken, checking } = this.state;

    switch (step) {
      case 1:
        return (
          <OrganizationDetails
            onSubmit={this.acceptOrganizationDetails}
          />
        );
      case 2:
        return (
          <PickSubdomain
            onSubmit={this.acceptSubdomain}
            isSubdomainTaken={isSubdomainTaken}
            checking={checking}
            isSubdomainValid={this.isSubdomainValid()}
            validateSubdomain={this.validateSubdomain}
          />
        );
      default:
        throw new Error('Unknown step');
    }
  };

  render() {
    return (
      <div className={styles.root}>
        <div className={styles.stepperContainer}>
          <Container fluid className={'justify-content-center'}>
            <Row className={'justify-content-center'}>
              <Col md={5} xs={12} className={styles.stprCmp}>
                <Stepper
                  activeStep={this.getActiveStep()}
                  steps={this.genSteps()}
                  completeColor={COLORS.PRIMARY}
                  activeColor={COLORS.PRIMARY}
                  size={25}
                />
              </Col>
            </Row>
          </Container>
        </div>
        <Container className={`${styles.form} justify-content-center d-flex`}>
          {this.renderStepContent()}
        </Container>
      </div>
    );
  }
}

export default connectRedux(CreateOrganizationForm);
