/**
 * @flow
 */
import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { authUser } from 'modules/user';
import { selectToken } from 'modules/token';

function mapStateToProps(state) {
  return {
    ...selectToken(state)
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    auth: authUser
  }, dispatch);
}
export default function connectRedux(component: React.ComponentType<*>) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(component);
}
