/**
 * @flow
 */
import * as React from 'react';
import Helmet from 'react-helmet';
import Header from './components/Header';
import SigninForm from './components/SigninForm';
import connect from './connect';

type Props = {
  auth: Function,
  isAuthenticated: boolean,
  token: string
};
class SigninContainer extends React.Component<Props> {
  handleSignin = (values) => {
    return new Promise((resolve, reject) => {
      this.props.auth({
        credentials: values,
        resolve,
        reject
      });
    });
  };

  render() {
    return (
      <div>
        <Helmet>
          <title>Sign in</title>
        </Helmet>
        <Header />
        <SigninForm
          onSubmit={this.handleSignin}
          isAuthenticated={this.props.isAuthenticated}
        />
      </div>
    );
  }
}

export default connect(SigninContainer);
