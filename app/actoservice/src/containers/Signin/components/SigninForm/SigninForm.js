/**
 * @flow
 */
import * as React from 'react';
import Redirect from 'react-router-dom/Redirect';
import Field from 'redux-form/lib/Field';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import Button from '@material-ui/core/Button';
import grey from '@material-ui/core/colors/grey';
import withStyles from '@material-ui/core/styles/withStyles';
import Progress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import connect from './connect';
import styles from './styles.scss';
import Link from 'react-router-dom/Link';
import { get } from 'http';

type Props = {
  valid: boolean,
  isAuthenticated: boolean,
  submitFailed: boolean,
  submitSucceeded: boolean,
  submitFailed: boolean,
  submitting: boolean,
  onSubmit: Function,
  handleSubmit: Function,
  classes: {
    [key: string]: string
  }
};

const renderTextField = ({
  input,
  label,
  meta: { touched, error, submitting },
  ...custom
}) => (
  <TextField
    label={label}
    error={touched && !!error}
    helperText={(touched && error) || undefined}
    disabled={submitting}
    {...input}
    {...custom}
  />
);

const newStyles = {
  secondaryText: {
    color: grey[700],
    fontSize: 13,
    '& span': {
      marginRight: 10
    }
  }
};

class SigninForm extends React.Component<Props> {
  get isButtonDisabled() {
    return this.props.submitting
      || !this.props.valid;
  }

  render() {
    const {
      submitting,
      submitSucceeded,
      submitFailed,
      classes,
      isAuthenticated
    } = this.props;
    return (
      <Grid container justify={'center'} className={styles.container}>
        {(submitSucceeded || isAuthenticated) && <Redirect to={'/'} />}
        <Grid item xs={12}>
          <Typography
            variant={'title'}
            align={'center'}
            paragraph
            gutterBottom
          >
            Sign in
          </Typography>
        </Grid>
        <Paper elevation={4} className={styles.paper}>
          <Grid item>
            <Field
              name={'email'}
              component={renderTextField}
              label={'Email'}
              margin={'normal'}
              fullWidth
            />
            <Field
              name={'password'}
              component={renderTextField}
              label={'Password'}
              margin={'normal'}
              fullWidth
            />
            <Button
              fullWidth
              disabled={!this.props.valid}
              onClick={this.props.handleSubmit}
            >
              {submitting ? (
                <Progress />
              ) : (
                <Typography>
                  Sign in
                </Typography>
              )}
            </Button>
            {!submitting && submitFailed && (
              <Typography align={'center'} color={'error'}>
                Wrong username or password
              </Typography>
            )}
          </Grid>
          <Grid item className={classes.secondaryText}>
            <span>
                Don't have account yet?
            </span>
            <Link to={'signup'}>
                Signup
            </Link>
          </Grid>
        </Paper>
      </Grid>
    );
  }
}

export default withStyles(newStyles)(connect(SigninForm));
