/**
 * @flow
 */
import * as React from 'react';
import isNil from 'lodash/isNil';
import set from 'lodash/set';
import size from 'lodash/size';
import { isEmail } from 'utils/validators';
import { reduxForm } from 'redux-form';

const FORM_NAME = 'signin';

type Values = {
  email?: string,
  password?: string
};
function validate(values: Values) {
  const errors = {};
  if (!isEmail(values.email)) {
    set(errors, 'email', 'Email is invalid');
  }
  if (isNil(values.email)) {
    set(errors, 'email', 'Email is Required');
  }
  if (size(values.password) < 4) {
    set(errors, 'password', 'Password length should be gt than 4 symbols');
  }
  if (isNil(values.password)) {
    set(errors, 'password', 'Password is required');
  }
  return errors;
}

export default function connectForm(Component: React.ComponentType<*>) {
  return reduxForm({
    form: FORM_NAME,
    validate,
  })(Component);
}
