/**
 * @flow
 */

import { connect } from 'react-redux';
import { selectTheme } from 'modules/themes';

function mapStateToProps(state, props) {
  return {
    theme: selectTheme(props.match.params.themeId)(state)
  };
}

export default function connectRedux(Component) {
  return connect(mapStateToProps)(Component);
}
