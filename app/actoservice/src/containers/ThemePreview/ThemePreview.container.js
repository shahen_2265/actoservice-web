/**
 * @flow
 */
import * as React from 'react';
import get from 'lodash/get';
import Helmet from 'react-helmet';
import Header from 'components/Header';
import ThemePreview from 'components/ThemePreview';
import styles from './styles.scss';

import connect from './connect';

type Props = {
  theme: {
    id: number,
    name: string
  }
};

class ThemePreviewContainer extends React.Component<Props> {
  render() {
    const { theme } = this.props;
    return (
      <div className={styles.root}>
        <Helmet>
          <title>
            Preview Theme {get(theme, 'name', '')}
          </title>
        </Helmet>
        <Header
          showLogo
          background
          style={{ marginBottom: 0 }}
        />
        <ThemePreview theme={theme} />
      </div>
    );
  }
}

export default connect(ThemePreviewContainer);
