/**
 * @flow
 */
import * as React from 'react';
import isEmpty from 'lodash/isEmpty';
import size from 'lodash/size';
import trim from 'lodash/trim';
import { isEmail } from 'utils/validators';
import { reduxForm } from 'redux-form';

const FORM_NAME = 'signup';

type Values = {
  name?: string,
  email?: string,
  phone?: string,
  password?: string,
  repeatPassword?: string
};
function validate(values: Values) {
  const errors: Values = {};
  const {
    name,
    email,
    password,
    repeatPassword
  } = values;

  if (isEmpty(trim(name))) {
    errors.name = 'Name is required';
  }

  if (isEmpty(trim(email))) {
    errors.email = 'Email is required';
  } else if (!isEmail(email)) {
    errors.email = 'Email is invalid';
  }

  if (size(password) < 4) {
    errors.password = 'Password length should be greater than 3 characters';
  }
  if (isEmpty(trim(password))) {
    errors.password = 'Password is required';
  } else if (password !== repeatPassword && !isEmpty(trim(repeatPassword))) {
    const msg = 'Passwords doesn\'t match';
    errors.password = msg;
    errors.repeatPassword = msg;
  }
  if (isEmpty(trim(repeatPassword))) {
    errors.repeatPassword = 'Please repeat password';
  }

  return errors;
}

export default function connectForm(Component: React.ComponentType<*>) {
  return reduxForm({
    form: FORM_NAME,
    validate,
  })(Component);
}
