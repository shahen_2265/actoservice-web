/**
 * @flow
 */
import * as React from 'react';
import Redirect from 'react-router-dom/Redirect';
import Field from 'redux-form/lib/Field';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Link from 'react-router-dom/Link';
import Progress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import connect from './connect';
import styles from './styles.scss';

type Props = {
  valid: boolean,
  isAuthenticated: boolean,
  submitFailed: boolean,
  submitSucceeded: boolean,
  submitting: boolean,
  onSubmit: Function,
  handleSubmit: Function,
  classes: {
    [key: string]: string
  }
};

const renderTextField = ({
  input,
  label,
  meta: { touched, error, submitting },
  ...custom
}) => (
  <TextField
    label={label}
    error={touched && !!error}
    helperText={(touched && error) || undefined}
    disabled={submitting}
    {...input}
    {...custom}
  />
);

class SignupForm extends React.Component<Props> {
  get isButtonDisabled() {
    return this.props.submitting
      || !this.props.valid;
  }
  render() {
    const { submitting, submitSucceeded, isAuthenticated } = this.props;
    return (
      <Grid container justify={'center'} className={styles.container}>
        {(submitSucceeded || isAuthenticated) && <Redirect to={'/dashboard'} />}
        <Grid item xs={12}>
          <Typography
            variant={'title'}
            align={'center'}
            paragraph
            gutterBottom
          >
            Sign Up
          </Typography>
        </Grid>
        <Paper elevation={4} className={styles.paper}>
          <Grid item>
            <Field
              name={'name'}
              component={renderTextField}
              label={'Name'}
              margin={'normal'}
              fullWidth
            />
            <Field
              name={'email'}
              component={renderTextField}
              label={'Email'}
              margin={'normal'}
              fullWidth
            />
            <Field
              name={'phone'}
              component={renderTextField}
              label={'Phone'}
              margin={'normal'}
              fullWidth
            />
            <Field
              name={'password'}
              component={renderTextField}
              label={'Password'}
              type={'password'}
              margin={'normal'}
              fullWidth
            />
            <Field
              name={'repeatPassword'}
              component={renderTextField}
              label={'Repeat Password'}
              type={'password'}
              margin={'normal'}
              fullWidth
            />
            <Button
              fullWidth
              disabled={this.isButtonDisabled}
              onClick={this.props.handleSubmit}
            >
              {submitting ? (
                <Progress />
              ) : (
                <Typography>
                  Sign Up
                </Typography>
              )}
            </Button>
          </Grid>
        </Paper>
      </Grid>
    );
  }
}

export default connect(SignupForm);
