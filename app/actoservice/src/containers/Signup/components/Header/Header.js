/**
 * @flow
 */
import * as React from 'react';
import HeaderComponent from 'components/Header';

export default function Header() {
  return (
    <HeaderComponent showLogo background />
  );
}
