/**
 * @flow
 */
import * as React from 'react';
import Helmet from 'react-helmet';
import Header from './components/Header';
import SignupForm from './components/SignupForm';
import connect from './connect';

type Props = {
  isAuthenticated: boolean,
  token: string,
  signup: (payload: { data: *, resolve: Function, reject: Function }) => void
}
class Signup extends React.Component<Props> {
  handleSubmit = (values: {}) => {
    return new Promise((resolve, reject) => {
      this.props.signup({
        data: values,
        resolve,
        reject
      });
    });
  };
  render() {
    const { isAuthenticated } = this.props;
    return (
      <div>
        <Helmet>
          <title>Sign up</title>
        </Helmet>
        <Header />
        <SignupForm
          onSubmit={this.handleSubmit}
          isAuthenticated={isAuthenticated}
        />
      </div>
    );
  }
}

export default connect(Signup);
