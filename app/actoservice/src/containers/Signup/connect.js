/**
 * @flow
 */
import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { signupUser } from 'modules/user';
import { selectToken } from 'modules/token';

function mapStateToProps(state) {
  return {
    ...selectToken(state)
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    signup: signupUser
  }, dispatch);
}

export default function connectRedux(Component: React.ComponentType<*>) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(Component);
}
