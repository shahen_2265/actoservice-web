/**
 * @flow
 */

import type { ComponentType } from 'react';
import { connect } from 'react-redux';
import { selectOrganizations, fetchOrganizations } from 'modules/organizations';
import { bindActionCreators } from 'redux';

function mapStateToProps(state) {
  return {
    ...selectOrganizations(state)
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchOrganizations
  }, dispatch);
}
export default function connectRedux(Component: ComponentType<*>) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(Component);
}
