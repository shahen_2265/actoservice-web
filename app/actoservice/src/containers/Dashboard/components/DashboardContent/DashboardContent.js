/**
 * @flow
 */
import * as React from 'react';
import { Col, Row, Container } from 'reactstrap';
import isEmpty from 'lodash/isEmpty';
import OrganizationCard from 'components/OrganizationCard';
import Text from 'components/UI/Text';

type Props = {
  loading: boolean,
  organizations: Array<*>,
  classes: {
    [key: string]: string
  }
};

function DashboardContent({
  organizations,
  loading,
}: Props) {
  return (
    <Container fluid>
      <Row>
        <Col className={'text-center'}>
          <Text>
            My organizations
          </Text>
        </Col>
      </Row>
      <Container className={'justify-content-center'}>
        {isEmpty(organizations) && !loading && (
          <div className={'text-center'}>
            Seems like you don't have any organiations yet
            {'\n'}
            Create your first organization !
          </div>
        )}
        {organizations.map(org => (
          <OrganizationCard
            key={org.id}
            organization={org}
          />
        ))}
      </Container>
    </Container>
  );
}

export default DashboardContent;
