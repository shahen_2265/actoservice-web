/**
 * @flow
 */
import * as React from 'react';
import Link from 'react-router-dom/Link';
import { Col } from 'reactstrap';
import Header from 'components/Header';
import DashboardContent from './components/DashboardContent';
import connect from './connect';

type Props = {
  history: *,
  organizations: Array<*>,
  organizationsLoading: boolean,
  fetchOrganizations: Function,
  classes: {
    fab: string
  }
};

class Dashboard extends React.Component<Props> {
  componentDidMount = () =>
    this.props.fetchOrganizations();

  render() {
    const {
      organizations,
      organizationsLoading
    } = this.props;
    return (
      <div>
        <Header
          showLogo
          background
        >
          <Col className={'text-right'}>
            <Link to={'create-organization'}>
              Create new
            </Link>
          </Col>
        </Header>
        <DashboardContent
          organizations={organizations}
          loading={organizationsLoading}
        />
      </div>
    );
  }
}

export default connect(Dashboard);
