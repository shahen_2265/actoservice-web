/**
 * @flow
 */
import * as React from 'react';
import { Col, Row, Container } from 'reactstrap';
import Helmet from 'react-helmet';
import Text from 'components/UI/Text';
import Header from 'components/Header';

export default function NotFound() {
  return (
    <div>
      <Helmet>
        <title>Not Found</title>
      </Helmet>
      <Header showLogo background />
      <Container fluid className={'justify-content-center'}>
        <Row>
          <Col className={'text-center'}>
            <Text>404 Not Found</Text>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
