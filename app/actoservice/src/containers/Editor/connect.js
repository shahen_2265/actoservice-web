/**
 * @flow
 */
import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import get from 'lodash/get';

import {
  fetchThemes,
  selectThemes
} from 'modules/themes';
import {
  fetchOrganizationScheme,
  selectOrganizationScheme,
  selectOrganizations,
  selectOrganizationById
} from 'modules/organizations';
import { parseScheme } from 'utils/schemeUtils';
import {
  fetchTemplate,
  updateTemplate,
  selectTemplate
} from 'modules/templates';

function mapStateToProps(state, props) {
  const organizationId = +get(props, 'match.params.orgId');

  const org = selectOrganizationById(organizationId)(state);
  const template = selectTemplate(org.path)(state);
  const themeScheme = selectOrganizationScheme(organizationId)(state) || {};

  return {
    ...selectThemes(state),
    ...selectOrganizations(state),
    currentOrganization: org,
    themeScheme,
    template,
    scheme: parseScheme(themeScheme, template.scheme)
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchThemes,
    fetchOrganizationScheme,
    fetchTemplate,
    uploadChanges: updateTemplate
  }, dispatch);
}

export default function connectRedux(component: React.ComponentType<*>) {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(component);
}
