/**
 * @flow
 */
import * as React from 'react';
import Header from 'components/Header';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AppsIcon from '@material-ui/icons/Apps';
import SaveIcon from '@material-ui/icons/Save';

import styles from './styles.scss';

type Props = {
  onMenuPress: Function,
  isExpired: boolean,
  hasChanges: boolean,
  todayExpires: boolean,
  onSaveChanges: Function,
  onCloseFtueDiscovery: Function,
  ftue: {
    apps: {
      active: boolean,
      seen: boolean
    }
  }
};

class EditorHeader extends React.Component<Props> {
  closeAppsFtue = () => {
    if (this.props.ftue.apps.active) {
      this.props.onCloseFtueDiscovery('apps');
    }
  };

  render() {
    const {
      onMenuPress,
      todayExpires,
      isExpired,
      hasChanges,
      onSaveChanges
    } = this.props;

    return (
      <Header
        showLogo
        logoRight
        background
        style={{ marginBottom: 0 }}
      >
        <Button onClick={onMenuPress}>
          <AppsIcon className={styles.icon} />
        </Button>
        {hasChanges && (
          <Button onClick={onSaveChanges}>
            <SaveIcon className={styles.icon} />
          Save
          </Button>
        )}
        {(todayExpires || isExpired) && (
          <Typography color={'textSecondary'}>
            {todayExpires
              ? 'Your active period will expire today'
              : 'Your plan expired'
            }
          </Typography>
        )}
      </Header>
    );
  }
}

export default EditorHeader;
