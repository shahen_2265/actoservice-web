// @flow

import * as React from 'react';
import get from 'lodash/get';
import isEqual from 'lodash/isEqual';
import Drawer from 'components/Drawer';
import ClientApp from 'components/ClientApp';
import type { UpdatesPayload } from 'components/ClientApp';
import { mixTemplateScheme } from 'utils/schemeUtils';
import Header from './components/EditorHeader';
import connect from './connect';

type PathDependFn = (params: { path: string }) => void;

type Props = {
  currentOrganization: {
    id: number,
    path: string,
    isExpired: boolean,
    isActive: boolean,
    todayExpires: boolean,
    [key: string]: mixed,
  },
  template: {
    scheme: {
      [key: string]: *
    },
    loading: boolean,
    error: ?{}
  },
  organizations: {},
  organizationsLoading: boolean,
  organizationsError: ?{},
  fetchOrganizationScheme: PathDependFn,
  fetchTemplate: PathDependFn,
  uploadChanges: (payload: {
    data: {},
    resolve: Function,
    reject: Function
  }) => void;
  scheme: {
    [key: string]: *
  },
  themeScheme: {
    [key: string]: *
  }
};

type State = {
  isDrawerOpen: boolean,
  hasChanges: boolean
};
class Editor extends React.Component<Props, State> {
  state = {
    isDrawerOpen: false,
    hasChanges: false
  };
  clientRef = React.createRef();

  componentDidMount() {
    const { currentOrganization } = this.props;
    this.props.fetchOrganizationScheme({
      path: currentOrganization.path
    });
    this.props.fetchTemplate({
      path: currentOrganization.path
    });
  }

  handleChanges = (payload: UpdatesPayload) => {
    const mix = mixTemplateScheme(
      this.props.template.scheme,
      this.props.currentOrganization.scheme
    );

    const hasChanges = !isEqual(mix, payload.result);

    if (hasChanges && !this.state.hasChanges) {
      this.setState({ hasChanges: true });
    }
    if (!hasChanges && this.state.hasChanges) {
      this.setState({ hasChanges: false });
    }
  };

  saveChanges = () => {
    const updates = this.clientRef.current.getChanges();
    return new Promise((resolve, reject) => {
      this.props.uploadChanges({
        data: {
          updates,
          organizationId: this.props.currentOrganization.id
        },
        resolve,
        reject
      });
    });
  };

  closeDrawer = () =>
    this.setState({ isDrawerOpen: false });

  openDrawer = () =>
    this.setState({ isDrawerOpen: true });

  render() {
    const {
      currentOrganization: organization,
      organizationsLoading
    } = this.props;

    return (
      <div>
        <Header
          onMenuPress={this.openDrawer}
          isExpired={organization.isExpired}
          todayExpires={organization.todayExpires}
          hasChanges={this.state.hasChanges}
          onSaveChanges={this.saveChanges}
        />
        <ClientApp
          organization={get(organization, 'path')}
          ref={this.clientRef}
          loading={organizationsLoading}
          onChangesReceived={this.handleChanges}
        />
        <Drawer
          open={this.state.isDrawerOpen}
          onClose={this.closeDrawer}
          onOpen={this.openDrawer}
          scheme={this.props.scheme}
        />
      </div>
    );
  }
}

export default connect(Editor);
