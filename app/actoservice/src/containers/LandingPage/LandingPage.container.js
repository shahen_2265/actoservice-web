/**
 * @flow
 */

import * as React from 'react';
import Helmet from 'react-helmet';
import { banner } from 'constants';
import ThemesShowCase from 'components/ThemesShowCase';

// import ThemesShowCase from 'components/ThemesShowCase';
import Header from './components/Header';
import Content from './components/Content';
import Footer from './components/Footer';
import connect from './connect';

const randomBanner = banner.randomImage();

const bannerHeight = '600px';

function generateBackgroundStyle(bnr: string) {
  return {
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
    backgroundImage: `url(${bnr})`,
    backgroundRepeat: 'no-repeat',
    display: 'inline-block',
    height: bannerHeight,
    position: 'absolute',
    width: '100%',
    filter: 'blur(2px)'
  };
}

class LandingPage extends React.PureComponent<*, *> {
  onThemeSelect = (id: number) => {
    if (this.props.isAuthenticated) {
      this.props.history.push(`/create-organization?theme=${id}`);
      return;
    }
    this.props.history.push(`/signin?return_url=create-organization?theme=${id}`);
  }
  handleStart = () => {
    if (this.props.isAuthenticated) {
      this.props.history.push('/dashboard');
      return;
    }
    this.props.history.push('/signin');
  };

  render() {
    return (
      <div>
        <Helmet>
          <title>Actoservice - Have the best, and be free</title>
        </Helmet>
        <div style={generateBackgroundStyle(randomBanner)} />
        <Header />
        <Content
          bannerHeight={bannerHeight}
          onGetStarted={this.handleStart}
          isAuthenticated={this.props.isAuthenticated}
        />
        <ThemesShowCase
          themes={this.props.themes}
          preview
          selectable
          onSelect={this.onThemeSelect}
        />
        <Footer />
      </div>
    );
  }
}

export default connect(LandingPage);
