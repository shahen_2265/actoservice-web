/**
 * @flow
 */
import { ComponentType } from 'react';
import { selectToken } from 'modules/token';
import { selectThemes } from 'modules/themes';
import { connect } from 'react-redux';

function mapStateToProps(state) {
  return {
    ...selectToken(state),
    ...selectThemes(state)
  };
}

export default function connectRedux(Component: ComponentType<*>) {
  return connect(
    mapStateToProps,
    null
  )(Component);
}
