/**
 * @flow
 */
import * as React from 'react';
import { Button, Col, Row } from 'reactstrap';
import Link from 'react-router-dom/Link';
import cx from 'classnames';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import HeaderComponent from 'components/Header';
import { isAuthenticated } from 'utils/auth';
import { withTranslations } from 'components/Translations';
import styles from './styles.scss';

type Props = {
  t: Function,
  lng: string,
  changeLanguage: Function
};

class Header extends React.Component<Props, *> {
  state = {
    anchorEl: null
  };

  selectLng = (lng: string) => {
    this.setState({ anchorEl: null });
    this.props.changeLanguage(lng);
  }

  toggleMenu = (e) => {
    this.setState({
      anchorEl: e.currentTarget
    });
  }

  render() {
    const { t, lng } = this.props;
    return (
      <HeaderComponent showLogo>
        <Col sm={2} className={'text-center align-self-center'}>
          <Link to={'features'}>
            {t('whyActoservice')}
          </Link>
        </Col>
        <Col sm={2} className={'text-center align-self-center'}>
          <Link to={'pricing'}>
            {t('pricing')}
          </Link>
        </Col>
        <Col sm={2} className={'text-center align-self-center'}>
          <Link to={'about'}>
            {t('aboutUs')}
          </Link>
        </Col>
        {isAuthenticated() && (
          <Col sm={2} className={'text-center align-self-center'}>
            <Link to={'dashboard'}>
              {t('myWebsite')}
            </Link>
          </Col>
        )}
        <Col sm={2} className={'text-center align-self-center'}>
          <Button
            id={'lng'}
            onClick={this.toggleMenu}
            className={cx(styles.whiteText, styles.link)}
          >
            {lng.toUpperCase()}
          </Button>
          <Menu
            anchorEl={this.state.anchorEl}
            open={Boolean(this.state.anchorEl)}
            onClose={this.toggleMenu}
          >
            <MenuItem onClick={() => this.selectLng('en')}>English</MenuItem>
            <MenuItem onClick={() => this.selectLng('ru')}>Русскии</MenuItem>
            <MenuItem onClick={() => this.selectLng('hy')}>Հայերեն</MenuItem>
          </Menu>
        </Col>
      </HeaderComponent>
    );
  }
}


export default withTranslations(Header);
