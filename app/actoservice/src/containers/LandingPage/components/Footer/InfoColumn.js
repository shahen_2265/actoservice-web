/**
 * @flow
 */
import * as React from 'react';
import { Link } from 'react-router-dom';
import styles from './styles.scss';

type Props = {
  title: string,
  links: Array<{
    title: string,
    link: string
  }>
};

export type DataSource = Props;

export default function InfoColumn({ title, links }: Props) {
  return (
    <div className={styles.linksColumn}>
      <span className={styles.columnTitle}>{title}</span>
      {links.map(item => (
        <Link to={item.link} key={item.link} className={styles.linkItem}>
          {item.title}
        </Link>
      ))}
    </div>
  );
}
