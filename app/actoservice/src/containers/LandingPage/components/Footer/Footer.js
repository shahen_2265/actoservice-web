/**
 * @flow
 */
import * as React from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import styles from './styles.scss';
import InfoColumn, { type DataSource } from './InfoColumn';

const rootClass = cx(
  'footer',
  styles.footer
);

const companyColumn: DataSource = {
  title: 'Company',
  links: [
    { title: 'About Us', link: 'about' },
    { title: 'Careers', link: 'careers' },
    { title: 'Blog', link: 'blog' },
  ]
};

const productColumn: DataSource = {
  title: 'Product',
  links: [
    { title: 'Why Actoservice?', link: 'features' },
    { title: 'Customer Stories', link: 'customer-stories' },
    { title: 'Pricing', link: 'pricing' },
    { title: 'Security', link: 'security' },
  ]
};

const resourcesColumn: DataSource = {
  title: 'Resources',
  links: [
    { title: 'Help Center', link: 'help' },
    { title: 'Guides', link: 'guides' },
    { title: 'Partners', link: 'partners' },
  ]
};

export default function Footer() {
  return (
    <div className={rootClass}>
      <InfoColumn
        title={companyColumn.title}
        links={companyColumn.links}
      />
      <InfoColumn
        title={productColumn.title}
        links={productColumn.links}
      />
      <InfoColumn
        title={resourcesColumn.title}
        links={resourcesColumn.links}
      />
    </div>
  );
}
