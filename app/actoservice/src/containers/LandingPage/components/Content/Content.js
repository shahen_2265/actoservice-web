/**
 * @flow
*/

import * as React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import cx from 'classnames';
import { slogan } from 'constants';
import { withTranslations } from 'components/Translations';
import styles from './styles.scss';

const containerStyle = cx('landing', styles.container);

type Props = {
  t: Function,
  bannerHeight: string,
  onGetStarted: Function,
  isAuthenticated: boolean
};

const randomSlogan = slogan.random();
const contentText = cx(styles.contentText);
const section2 = cx(styles.section2);
const section3 = cx(styles.section3);

function Content({
  t,
  onGetStarted,
  isAuthenticated,
  bannerHeight
}: Props) {
  return (
    <div className={styles.root}>
      <div className={containerStyle}>
        <div className={contentText}>
          <h5>{t(randomSlogan)}</h5>
        </div>
      </div>
      <div style={{ height: bannerHeight }} />
      <div className={section2}>
        <h3>Who we are</h3>
        <span>A hounded of people trusting us their website</span>
      </div>
      <div className={section3}>
        <div>
          <h3>Try it for free</h3>
          {!isAuthenticated && (
            <span>
            Already using Actoservice ?
              <Link to={'/signin'} className={styles.link}>
              Sign in!
              </Link>
            </span>
          )}
        </div>
        <Button
          color={'primary'}
          className={styles.getStartedBtn}
          onClick={onGetStarted}
        >
        GET STARTED
        </Button>
      </div>
    </div>
  );
}

export default withTranslations(Content);
