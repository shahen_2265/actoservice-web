/**
 * @flow
 */
import BaseAPI from './base.api';

class UsersAPI extends BaseAPI {
  constructor() {
    super('users');
  }
}

export default new UsersAPI();
