/**
 * @flow
 */

import BaseAPI from './base.api';

class TemplatesAPI extends BaseAPI {
  constructor() {
    super('templates');
  }

  fetchTemplate = (path: string) =>
    this.get(`?organization=${path}`);

  update = (updates: *) =>
    this.put('', updates);
}

export default new TemplatesAPI();
