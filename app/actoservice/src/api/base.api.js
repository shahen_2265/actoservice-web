import axios from 'axios';
import {
  AUTH_LOCAL_STORAGE_KEY,
  TOKEN_NAME
} from 'constants';

const ADMIN_HOST = process.env.NODE_ENV === 'production'
  ? 'https://www.actoservice.com'
  : 'http://0.0.0.0:3000';

const BASE_HOST = ADMIN_HOST;
const API_V = 'v1';
const API_HOST = `${BASE_HOST}/api/${API_V}`;

const updateToken = require('modules/token').updateToken;

class BaseAPI {
  constructor(resource) {
    this.resource = resource;
    this.http = axios.create({
      baseURL: `${API_HOST}/${resource}`,
      timeout: 5000
    });

    this.http.interceptors.request.use(this._requestInterceptor);
    this.http.interceptors.response.use(undefined, this._retryFailedRequest);
    this.http.interceptors.response.use(this._responseInterceptor);
  }

  retryFailedRequest = (err) => {
    if (err.status === 503 && err.config && err.config.__retryCount < 4) {
      err.config.__retryCount = (err.config.__retryCount || 0) + 1;
      return axios(err.config);
    }
    throw err;
  }

  _responseInterceptor = (response) => {
    console.log(response);
    console.log(response.headers[TOKEN_NAME.toLowerCase()]);
    const newToken = response.headers[TOKEN_NAME.toLowerCase()];
    if (newToken) {
      window.localStorage.setItem(AUTH_LOCAL_STORAGE_KEY, newToken);
      updateToken(newToken);
    }
    return response;
  };

  _requestInterceptor = config => ({
    ...config,
    headers: {
      ...config.headers,
      common: {
        ...config.headers.common,
        [TOKEN_NAME]: window.localStorage.getItem(AUTH_LOCAL_STORAGE_KEY)
      }
    }
  });

  get = (path = '', ...args) =>
    this.http.get(path, ...args);

  post = (path = '', body) =>
    this.http.post(path, body);

  put = (path = '', body) =>
    this.http.put(path, body);
}

export default BaseAPI;
