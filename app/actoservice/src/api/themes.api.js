/**
 * @flow
 */
import BaseAPI from './base.api';
import { serializeParam } from 'utils/apiUtils';

class ThemesAPI extends BaseAPI {
  constructor() {
    super('themes');
  }
  fetchShowCase = () =>
    this.get(serializeParam({ showCase: true }));

  fetchAll = ({ prev, limit } = {}) =>
    this.get(serializeParam({ prev, limit }))
}

export default new ThemesAPI();
