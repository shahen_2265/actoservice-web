import organizations from './organizations.api';
import auth from './auth.api';
import templates from './templates.api';
import themes from './themes.api';

export default {
  organizations,
  auth,
  templates,
  themes
};
