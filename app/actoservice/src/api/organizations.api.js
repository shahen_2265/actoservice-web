/**
 * @flow
 */

import BaseAPI from './base.api';

class OrganizationsAPI extends BaseAPI {
  constructor() {
    super('organizations');
  }

  create = (payload: {}) =>
    this.post('', payload);

  getOrganizations = () =>
    this.get('');

  checkSubdomain = (subdomain: string) =>
    this.get(undefined, { params: { subdomain } });

  getScheme = (path: string) =>
    this.get(`${path}/theme?object=scheme.json`);
}

export default new OrganizationsAPI();
