/**
 * @flow
 */
import BaseAPI from './base.api';

type Credentials = {
  email: string,
  password: string
};

type SignupCredentials = {
  name: string,
  email: string,
  phone: string,
  password: string,
};

class AuthApi extends BaseAPI {
  constructor() {
    super('auth');
  }

  authenticate = (credentials: Credentials) =>
    this.post('/signin', credentials);

  signup = (credentials: SignupCredentials) =>
    this.post('/signup', credentials);
}

export default new AuthApi();
