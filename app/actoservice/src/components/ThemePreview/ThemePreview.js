/**
 * @flow
 */
import React from 'react';
import isEmpty from 'lodash/isEmpty';
import { HOST } from 'constants';
import styles from './styles.scss';

type Props = {
  theme: {
    id: number,
    name: string
  }
}
class ThemePreview extends React.PureComponent<Props> {
  state = {
    loaded: false,
    error: false
  };
  previewRef = React.createRef();

  onLoad = () =>
    this.setState({ loaded: true });

  onError = () =>
    this.setState({ error: true });

  renderSplash = () => (
    <div className={styles.splashLoader}>
      {this.state.error ?
        <span style={{ color: 'white' }}>
                Something went wrong, Please try later
        </span> :
        <span style={{ color: 'white' }}>
                Preparing {'\n'}
          <b>{this.props.theme.name}</b> preview...
        </span>
      }
    </div>
  )
  render() {
    const { theme } = this.props;
    const { loaded, error } = this.state;

    if (isEmpty(theme)) {
      return this.renderSplash();
    }
    return (
      <div className={styles.previewParentNode}>
        <iframe
          ref={this.previewRef}
          src={`http://${HOST}/_/preview?theme=${theme.name}`}
          className={styles.previewNode}
          onLoad={this.onLoad}
        />
        {(!loaded || error) && (
          <div className={styles.splashLoader}>
            {error ?
              <span style={{ color: 'white' }}>
                Something went wrong, Please try later
              </span> :
              <span style={{ color: 'white' }}>
                Preparing {'\n'}
                <b>{theme.name}</b> preview...
              </span>
            }
          </div>
        )}
      </div>
    );
  }
}
export default ThemePreview;
