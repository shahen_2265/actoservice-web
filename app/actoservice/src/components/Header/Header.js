// @flow

import * as React from 'react';
import { Col, Row, Container } from 'reactstrap';
import isEmpty from 'lodash/isEmpty';
import cx from 'classnames';
import Link from 'react-router-dom/Link';
import logoTransparent from 'assets/img/logo_transparent.png';
import logoWhiteBackground from 'assets/img/logo_white_background.jpg';

import styles from './styles.scss';

type Props = {
  title: string,
  pages: Array<string>,
  background: boolean,
  showLogo: boolean,
  style: {},
  children: typeof React.Element
};

const centerContent = cx(
  styles.container,
  styles.headerHeight,
  'align-items-center',
);

const columnsClassNames = cx(
  'justify-content-center',
  styles.columns
);

class Header extends React.Component<Props> {
  state = {
    showBackground: false,
    collapsed: false
  };

  componentDidMount() {
    this.onScroll();
    window.addEventListener('scroll', this.onScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll);
  }

  onScroll = () => {
    const dirtyState = {};

    if (document.documentElement.scrollTop > 400 && !this.state.collapsed) {
      dirtyState.collapsed = true;
    }
    if (document.documentElement.scrollTop < 400 && this.state.collapsed) {
      dirtyState.collapsed = false;
    }
    if (document.documentElement.scrollTop > 200 && !this.state.showBackground) {
      dirtyState.showBackground = true;
    }
    if (document.documentElement.scrollTop < 200 && this.state.showBackground) {
      dirtyState.showBackground = false;
    }
    if (!isEmpty(dirtyState)) {
      this.setState(dirtyState);
    }
  };

  render() {
    const { showBackground, collapsed } = this.state;
    const {
      background,
      showLogo,
      children,
      style
    } = this.props;

    return (
      <Container
        fluid
        className={
          cx(centerContent, {
            [styles.backgroundIncluded]: showBackground,
            [styles.collapsedHeader]: collapsed,
            [styles.containerBackground]: background
          })
        }
        style={style}
      >
        <Row className={'w-100 align-items-center header-row'}>
          <Col xs={2} className={columnsClassNames}>
            {showLogo && (
              <Link className={'header-nav'} to='/'>
                <img
                  src={background ? logoWhiteBackground : logoTransparent}
                  className={styles.logo}
                />
              </Link>
            )}
          </Col>
          <Col>
            <Row className={'justify-content-end'}>
              {React.Children
                .toArray(children)
                .map(child => React.cloneElement(child))
              }
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Header;
