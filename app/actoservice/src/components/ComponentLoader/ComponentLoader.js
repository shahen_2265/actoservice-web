import React from 'react';

export default function ComponentLoader(props) {
  if (props.error) {
    console.log(props.error);
    return <div>Error! <button onClick={props.retry}>Retry</button></div>;
  }
  return <div>Loading...</div>;
}
