/**
 * global $PropertyType
 *
 * @flow
 */
import * as React from 'react';
import get from 'lodash/get';
import mergeWith from 'lodash/mergeWith';
import { HOST } from 'constants';
import styles from './styles.scss';

type State = {
  loaded: boolean,
  error: boolean
};

export type UpdatesPayload = {
  updates: {
    [key: string]: *
  },
  result: {
    [key: string]: *
  }
};

type Updates = {
  action?: string,
  payload: {
    [key: string]: *
  },
};


type Props = {
  organization: string,
  loading: boolean,
  onError?: Function,
  onChangesReceived: (changes: $PropertyType<Updates, 'payload'>) => void
};

const actionIdentifier = '__ACTOSERVICE__ACTION__';
const notifyUpdatesAction = 'AS:sdk:notify_updates';

class ClientApp extends React.Component<Props, State> {
  state = { loaded: false, error: false };
  appRef = React.createRef();

  updates = [];
  result = null;

  componentDidMount() {
    this.registerListener();
  }

  componentWillUnmount() {
    this.dispose();
  }

  onNotifyUpdates = (payload) => {
    this.updates.push(payload.updates);
    this.result = payload.result;

    this.props.onChangesReceived(payload);
  };

  getChanges = () =>
    mergeWith(
      ...this.updates,
      (objVal, sourceVal) =>
        (Array.isArray(sourceVal) ? sourceVal : undefined)
    );

  registerListener = () =>
    window.onmessage = this.onMessageReceived;

  dispose = () =>
    window.onmessage = null;

  onMessageReceived = ({ data }: { data: Updates }) => {
    const action = get(data, actionIdentifier);
    const payload = get(data, 'payload');

    if (!action) {
      return;
    }

    switch (action) {
      case notifyUpdatesAction:
        this.onNotifyUpdates(payload);
        break;
      default:
        console.warn('Dispatched from unknown source');
    }
  };

  onLoadIframe = () =>
    this.setState({ loaded: true });

  onError = () =>
    this.setState({ error: true });

  sendUpdates = (updates: {}) => {
    if (!this.appRef) {
      console.warn('Client app was destroyed');
      return;
    }
    this.appRef.current.contentWindow.postMessage({
      [actionIdentifier]: true,
      payload: updates
    }, '*');
  };

  render() {
    const { loaded, error } = this.state;
    const {
      organization,
      loading: organizationLoading
    } = this.props;
    return (
      <div className={styles.clientContainer}>
        <iframe
          ref={this.appRef}
          src={`http://${organization}.${HOST}`}
          className={styles.clientApp}
          onLoad={this.onLoadIframe}
        />
        {(!loaded || organizationLoading || error) && (
          <div className={styles.splashLoader}>
            <span style={{ color: 'white' }}>
              {error
                ? 'Something went wrong, Please try later'
                : 'Loading your website...'
              }
            </span>
          </div>
        )}
      </div>
    );
  }
}

export default ClientApp;
