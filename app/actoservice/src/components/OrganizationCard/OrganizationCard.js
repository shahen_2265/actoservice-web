/**
 * @flow
 */
import * as React from 'react';

import {
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
} from 'reactstrap';
import cx from 'classnames';
import Button from 'components/UI/Button';
import Text from 'components/UI/Text';
import Link from 'react-router-dom/Link';
import { HOST } from 'constants';
import styles from './styles.scss';

type Props = {
  organization: {
    id: number,
    name: string,
    description: string,
    path: string,
    theme: {
      imageUrl: string,
      name: string
    }
  },
  classes: {
    [key: string]: string
  }
};

const containerClasses = cx('justify-content-center', styles.row);

function OrganizationCard({ organization }: Props) {
  return (
    <Row className={containerClasses}>
      <Col xs={8}>
        <Card>
          <CardImg
            top
            className={styles.cardImg}
            src={organization.theme.imageUrl}
          />
          <CardBody>
            <CardTitle>
              <Text>
                {organization.name}
              </Text>
            </CardTitle>
            <CardText>
              <Text size={Text.sizes.sm} className={'text-secondary'}>
                {organization.description}
              </Text>
            </CardText>
            <Row>
              <Col xs={1}>
                <Link
                  to={`/editor/${organization.id}/web`}
                  className={`${styles.edit} btn`}
                >
                  Edit
                </Link>
              </Col>
              <Col>
                <Button
                  type={Button.types.transparent}
                  onClick={() => window.open(`${organization.path}.${HOST}`)}
                >
                  Open
                </Button>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </Col>
    </Row>
  );
}

export default OrganizationCard;
