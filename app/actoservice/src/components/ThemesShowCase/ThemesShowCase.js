/**
 * @flow
*/
import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import ThemeCard from 'components/ThemeCard';
import Text from 'components/UI/Text';
import styles from './styles.scss';

type Props = {
  preview: boolean,
  selectable: boolean,
  onSelect: (id: number) => void,
  themes: {
    id: number,
    name: string,
    description?: string,
    imageUrl: string
  },
};
export default function ThemesShowCase({
  themes,
  preview,
  selectable,
  onSelect
}: Props) {
  return (
    <Container fluid>
      <Row className={styles.titleRow}>
        <Col className={'justify-content-center align-items-center d-flex'}>
          <Text>Our Themes Showcase</Text>
        </Col>
      </Row>
      <Row className={'justify-content-center'}>
        {themes.map(theme => (
          <ThemeCard
            theme={theme}
            key={theme.id}
            preview={preview}
            onSelect={selectable ? onSelect : null}
          />
        ))}
        {themes.map(theme => (
          <ThemeCard
            theme={theme}
            key={theme.id}
            preview={preview}
            onSelect={selectable ? onSelect : null}
          />
        ))}
        {themes.map(theme => (
          <ThemeCard
            theme={theme}
            key={theme.id}
            preview={preview}
            onSelect={selectable ? onSelect : null}
          />
        ))}
        {themes.map(theme => (
          <ThemeCard
            theme={theme}
            key={theme.id}
            preview={preview}
            onSelect={selectable ? onSelect : null}
          />
        ))}
        {themes.map(theme => (
          <ThemeCard
            theme={theme}
            key={theme.id}
            preview={preview}
            onSelect={selectable ? onSelect : null}
          />
        ))}
        {themes.map(theme => (
          <ThemeCard
            theme={theme}
            key={theme.id}
            preview={preview}
            onSelect={selectable ? onSelect : null}
          />
        ))}
      </Row>
    </Container>
  );
}
