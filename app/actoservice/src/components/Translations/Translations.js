/**
 * @flow
 */
import * as React from 'react';
import get from 'lodash/get';
import languages from 'i18N';

const context = React.createContext();

type State = {
  language: string
};

type Props = {
  children: React.ComponentType<*>
};

type ConsumerRenderProps = {
  language: string,
  changeLanguage: (lng: string) => void,
  translate: (str: string) => string
};

export class TranslationsProvider extends React.Component<Props, State> {
  state = {
    language: window.navigator.language.split('-')[0]
  };
  changeLanguage = (language: string) =>
    this.setState({ language });

  translate = (key: string) =>
    get(languages[this.state.language], key);

  render() {
    const { Provider } = context;
    const value: ConsumerRenderProps = {
      language: this.state.language,
      changeLanguage: this.changeLanguage,
      translate: this.translate
    };

    return (
      <Provider value={value}>
        {React.Children.only(this.props.children)}
      </Provider>
    );
  }
}

export function withTranslations(Component: React.ComponentType<*>) {
  const { Consumer } = context;

  class WithTranslations extends React.Component<*> {
    render() {
      return (
        <Consumer>
          {
            ({ changeLanguage, translate, language }: ConsumerRenderProps) =>
              <Component
                {...this.props}
                changeLanguage={changeLanguage}
                t={translate}
                lng={language}
              />
          }
        </Consumer>
      );
    }
  }
  WithTranslations.displayName = `${Component.name}`;

  return WithTranslations;
}
