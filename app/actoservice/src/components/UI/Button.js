/**
 * @flow
 */
import * as React from 'react';
import cx from 'classnames';
import { Button as BButton } from 'reactstrap';
import styles from './button.styles.scss';

type Props = {
  type: 'primary' | 'warning' | 'success',
  className: string
};

const types = {
  primary: 'primary',
  warning: 'warning',
  transparent: 'transparent'
};

function Button({ type, className, ...rest }: Props) {
  const _type = type || types.primary;

  const combinedClasses = cx(className, styles[`btn-${_type}`]);

  return (
    <BButton className={combinedClasses} {...rest} />
  );
}

Button.types = types;

export default Button;
