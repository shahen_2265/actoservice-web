/**
 * @flow
 */
import * as React from 'react';
import cx from 'classnames';
import styles from './text.styles.scss';

type Props = {
  size: 1 | 2 | 3 | 4 | 5,
  className: string,
  children: React.ReactChild,
  rest: *
};

const sizes = {
  xs: 1,
  sm: 2,
  md: 3,
  lg: 4,
  xlg: 5
};

function Text({
  size,
  className,
  children,
  ...rest
}: Props) {
  const _size = size || sizes.md;

  const classes = cx(styles[`text-${_size}`], className);

  return (
    <span className={classes} {...rest}>
      {children}
    </span>
  );
}

Text.sizes = sizes;

export default Text;
