/**
 * @flow
 */

import * as React from 'react';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Brush from '@material-ui/icons/Brush';
import Add from '@material-ui/icons/Add';
import { Typography } from '@material-ui/core';
import DrawerItem from './DrawerItem';

type Props = {
  open: boolean,
  onClose: Function,
  onOpen: Function,
  scheme: {
    [key: string]: any
  },
  classes: {
    [key: string]: string
  }
};

const styles = {
  list: {
    width: 250,
  },
  button: {
    paddingLeft: 20,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  icon: {
    marginRight: 5,
  },
  fullList: {
    width: 'auto',
  }
};

function Drawer({
  open,
  onClose,
  onOpen,
  classes,
  scheme
}: Props) {
  return (
    <SwipeableDrawer
      open={open}
      onClose={onClose}
      onOpen={onOpen}
    >
      <div className={classes.list}>
        {scheme.map(editable => (
          <List key={editable.path}>
            <DrawerItem
              item={editable}
              onPress={onClose}
              classes={classes}
            />
            <Divider />
          </List>
        ))}
      </div>
    </SwipeableDrawer>
  );
}

export default withStyles(styles)(Drawer);
