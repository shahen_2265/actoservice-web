/**
 * @flow
 */
import * as React from 'react';
import get from 'lodash/get';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

type Item = {
  key: string,
  description?: string,
  value: string
};

type Props = {
  item: Item,
  onPress: (item: Item) => void,
  classes: {}
};
export default function DrawerItem({ item, onPress, classes }: Props) {
  return (
    <div>
      <Button
        fullWidth
        className={classes.button}
        onClick={() => onPress(item)}
      >
        <Typography variant={'title'}>
          {item.key}
        </Typography>
        <Typography variant={'subheading'}>
          {item.description}
        </Typography>
      </Button>
    </div>
  );
}
