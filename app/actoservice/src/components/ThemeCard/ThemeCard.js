/**
 * @flow
 */
import * as React from 'react';

import {
  Col,
  Card,
  CardText,
  CardBody,
  CardTitle,
} from 'reactstrap';
import Link from 'react-router-dom/Link';
import Text from 'components/UI/Text';
import Button from 'components/UI/Button';
import styles from './styles.scss';

type Props = {
  onSelect: (id: number) => void,
  preview: boolean,
  theme: {
    id: number,
    name: string,
    description: string,
    imageUrl: string,
    name: string
  },
};

function ThemeCard({
  theme,
  onSelect,
  preview,
}: Props) {
  return (
    <Col xs={12} sm={6} md={4} lg={3}>
      <Card>
        <div
          style={{
            background: `url(${theme.imageUrl})`,
            backgroundSize: 'contain',
            width: 'inherit',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            height: 300
          }}
          className={styles.cardImg}
        />
        <CardBody>
          <CardTitle>
            <Text>
              {theme.name}
            </Text>
          </CardTitle>
          <CardText>
            <Text size={Text.sizes.sm} className={'text-secondary'}>
              {theme.description}
            </Text>
          </CardText>
          {onSelect && (
            <Button onClick={() => onSelect(theme.id)}>Choose</Button>
          )}
          {preview && (
            <Link to={`/theme/${theme.id}/preview`} className={styles.previewLink}>
              Preview
            </Link>
          )}
        </CardBody>
      </Card>
    </Col>
  );
}

export default ThemeCard;
