// @flow
import 'styles/main.scss';
import 'styles/fonts.scss';

import React from 'react';
import Router from 'react-router-dom/BrowserRouter';
import Route from 'react-router-dom/Route';
import Switch from 'react-router-dom/Switch';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { makeComponentLoadable } from 'utils/routerUtils';
import { fetchOrganizations } from 'modules/organizations';
import { fetchThemes } from 'modules/themes';
import { selectToken } from 'modules/token';

type Props = {
  isAuthenticated: boolean,
  token: ?string,
  fetchOrganizations: Function,
  fetchThemes: Function
};

const loadEditor = () =>
  import('containers/Editor');

const loadLandingPage = () =>
  import('containers/LandingPage');

const loadSignupPage = () =>
  import('containers/Signup');

const loadDashboard = () =>
  import('containers/Dashboard');

const loadNotFound = () =>
  import('containers/NotFound');

const loadSigninPage = () =>
  import('containers/Signin');

const loadCreateOrganization = () =>
  import('containers/CreateOrganization');

const loadThemePreview = () =>
  import('containers/ThemePreview');

class Root extends React.Component<Props> {
  componentDidMount() {
    this.props.fetchThemes({ showCase: true });
    if (this.props.isAuthenticated) {
      this.props.fetchOrganizations();
    }
  }
  render() {
    const { isAuthenticated } = this.props;
    return (
      <Router>
        <Switch>
          <Route
            path={'/'}
            exact
            component={makeComponentLoadable(loadLandingPage)}
          />
          <Route
            exact
            path={'/signup'}
            component={makeComponentLoadable(loadSignupPage)}
          />
          <Route
            exact
            path={'/signin'}
            component={makeComponentLoadable(loadSigninPage)}
          />
          {isAuthenticated && (
            <Route
              exact
              path={'/editor/:orgId/web'}
              component={makeComponentLoadable(loadEditor)}
            />
          )}
          {isAuthenticated && (
            <Route
              exact
              path={'/create-organization'}
              component={makeComponentLoadable(loadCreateOrganization)}
            />
          )}
          {isAuthenticated && (
            <Route
              exact
              path={'/dashboard'}
              component={makeComponentLoadable(loadDashboard)}
            />
          )}
          <Route
            exact
            path={'/theme/:themeId/preview'}
            component={makeComponentLoadable(loadThemePreview)}
          />
          <Route component={makeComponentLoadable(loadNotFound)} />
        </Switch>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...selectToken(state)
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchOrganizations,
    fetchThemes
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Root);
