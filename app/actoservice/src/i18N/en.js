export default {
  whyActoservice: 'Why Actoservice?',
  pricing: 'Pricing',
  aboutUs: 'About Us',
  myWebsite: 'My Website',
  slogans: {
    slogan_1: 'Create your success with us',
    slogan_2: 'Have the best, and be free...',
    slogan_3: 'Make your future with us'
  }
};
