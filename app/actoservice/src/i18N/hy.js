export default {
  whyActoservice: 'Ինչու Actoservice?',
  pricing: 'Վճարում',
  aboutUs: 'Մեր մասին',
  myWebsite: 'Իմ կայքը',
  slogan: {
    1: 'Ունեցեք լավագույնը',
    2: 'Ու եղեք',
    3: 'Ազատ'
  }
};
