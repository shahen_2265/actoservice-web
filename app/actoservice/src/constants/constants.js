export const AUTH_LOCAL_STORAGE_KEY = 'actoservice_auth_token';
export const TOKEN_NAME = 'Authorization';

export const COLORS = {
  PRIMARY: '#0a6c8f'
};

export const SUBDOMAIN_INVALIDATOR = /[ !@#$%^&*()+\=\[\]{};':"\\|,.<>\/?]/;
export const HOST = process.env.NODE_ENV === 'production'
  ? 'actoservice.com'
  : 'localhost.com:3000';

const bannerImages = [
  'https://i.pinimg.com/originals/28/3d/f9/283df9fbdb3537211908b5ae21756124.jpg',
  'http://armenia-tour.am/wp-content/uploads/2015/12/armenia-yerevan-hraparak-hayk-barseghyans-wallpaper-1080p-HD-2.jpg'
];
const sloganTexts = [
  'slogans.slogan_1',
  'slogans.slogan_2',
  'slogans.slogan_3'
];

export const slogan = {
  texts: sloganTexts,
  random: () => sloganTexts[Math.floor(Math.random() * sloganTexts.length)]
};

export const banner = {
  images: bannerImages,
  randomImage: () => bannerImages[Math.floor(Math.random() * bannerImages.length)]
};
