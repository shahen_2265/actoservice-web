/**
 * @flow
 */
import { createSelector } from 'reselect';

export const selectThemes = createSelector(
  state => state.themes,
  themes => ({
    themesLoading: themes.loading,
    themesError: themes.error,
    themes: themes.data
  })
);

export const selectTheme = (id: number) =>
  createSelector(
    selectThemes,
    ({ themes }) => themes.find(theme => +theme.id === +id) || {}
  );
