/**
 * @flow
 */
import {
  takeEvery,
  ForkEffect,
  call,
  put,
  all,
  fork
} from 'redux-saga/effects';
import { createReducer } from 'redux-act';
import get from 'lodash/get';
import { createAPIActions } from 'utils/reduxUtils';
import Api from 'api';

export const [
  fetchThemes,
  fetchThemesSuccess,
  fetchThemesFailed
] = createAPIActions('Fetch themes');

const initialState = {
  data: [],
  loading: false,
  error: null
};

const onFetchThemes = state =>
  ({ ...state, loading: true });

const onFetchThemesSuccess = (state, payload) =>
  ({ ...state, loading: false, data: payload });

const onFetchThemesFailed = (state, e) =>
  ({ ...state, loading: false, error: e });

export default createReducer({
  [fetchThemes]: onFetchThemes,
  [fetchThemesSuccess]: onFetchThemesSuccess,
  [fetchThemesFailed]: onFetchThemesFailed
}, initialState);

function* processFetchThemes({ payload }) {
  const limit = get(payload, 'limit');
  const prev = get(payload, 'prev');
  const showCase = get(payload, 'showCase');

  const fn = showCase
    ? 'fetchShowCase'
    : 'fetchAll';

  try {
    const response = yield call(Api.themes[fn], { limit, prev });
    yield put(fetchThemesSuccess(response.data));
  } catch (e) {
    yield put(fetchThemesFailed(e.data));
  }
}

export function* fetchThemesWatcher(): ForkEffect {
  yield takeEvery(fetchThemes, processFetchThemes);
}

export function* themesSagas(): ForkEffect {
  yield all([
    fork(fetchThemesWatcher)
  ]);
}
