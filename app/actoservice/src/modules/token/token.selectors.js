/**
 * @flow
 */
import { createSelector } from 'reselect';

export const selectToken = createSelector(
  state => state.token,
  token => ({
    token,
    isAuthenticated: !!token
  })
);

