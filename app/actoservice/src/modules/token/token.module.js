/**
 * @flow
 */
import { createReducer, createAction } from 'redux-act';
import { AUTH_LOCAL_STORAGE_KEY } from 'constants';

export const updateToken = createAction('Update token');
const initialState = window.localStorage.getItem(AUTH_LOCAL_STORAGE_KEY);

const onUpdateToken = (state, token) => token;

export default createReducer({
  [updateToken]: onUpdateToken
}, initialState);
