/**
 * @flow
 */
import {
  takeEvery,
  ForkEffect,
  call,
  put,
  all,
  fork
} from 'redux-saga/effects';
import get from 'lodash/get';
import { createReducer } from 'redux-act';
import { createAPIActions } from 'utils/reduxUtils';
import Api from 'api';

type AuthPayload = {
  resolve: Function,
  reject: Function,
  credentials: {
    email: string,
    password: string
  }
};

export const [
  authUser,
  authUserSuccess,
  authUserFailed
] = createAPIActions('Authenticate user');

export const [
  signupUser,
  signupUserSuccess,
  signupUserFailed
] = createAPIActions('Signup user');

const onAuth = state =>
  ({ ...state, loading: true, error: null });

const onAuthSuccess = (state, { user }) =>
  ({ ...state, loading: false, data: user });

const onAuthFailed = (state, payload) =>
  ({ ...state, loading: false, error: get(payload, 'response.data.message[0]') });

const onSignup = state =>
  ({ ...state, loading: true });

const onSignupSuccess = (state, user) =>
  ({ ...state, loading: false, data: user });

const onSignupFailed = (state, error) =>
  ({ ...state, loading: false, error });

const initialState = {
  data: {},
  loading: false,
  error: null
};

export default createReducer({
  [authUser]: onAuth,
  [authUserSuccess]: onAuthSuccess,
  [authUserFailed]: onAuthFailed,

  [signupUser]: onSignup,
  [signupUserSuccess]: onSignupSuccess,
  [signupUserFailed]: onSignupFailed
}, initialState);

function* processAuth({ payload }: { payload: AuthPayload }) {
  const {
    credentials,
    resolve,
    reject
  } = payload;
  try {
    const result = yield call(Api.auth.authenticate, credentials);
    const user = get(result, 'data');
    yield put(authUserSuccess(user));
    yield resolve(user);
  } catch (e) {
    yield put(authUserFailed(e));
    yield reject(e);
  }
}

function* processSignup({ payload: { data, resolve, reject } }) {
  try {
    const response = yield call(Api.auth.signup, data);
    yield put(signupUserSuccess(response.data));
    yield resolve(response.data);
  } catch (e) {
    yield put(signupUserFailed(e));
    yield reject(e);
  }
}

function* watchSignup(): ForkEffect {
  yield takeEvery(signupUser, processSignup);
}

function* watchAuth(): ForkEffect {
  yield takeEvery(authUser, processAuth);
}

export function* userSagas(): ForkEffect {
  yield all([
    fork(watchAuth),
    fork(watchSignup)
  ]);
}
