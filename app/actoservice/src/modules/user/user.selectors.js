import { createSelector } from 'reselect';

export const selectUser = createSelector(
  state => state.user.data,
  result => result
);

export const selectUserError = createSelector(
  state => state.user.error,
  result => result
);
