/**
 * @flow
 */
import { takeEvery, ForkEffect, call, put, all, fork } from 'redux-saga/effects';
import { createReducer } from 'redux-act';
import uniqBy from 'lodash/uniqBy';
import { createAPIActions } from 'utils/reduxUtils';
import get from 'lodash/get';
import Api from 'api';

const initialState = {
  data: [],
  loading: false,
  error: null
};

export const [
  createOrganization,
  createOrganizationSuccess,
  createOrganizationFailed
] = createAPIActions('Create Organization');

export const [
  fetchOrganizations,
  fetchOrganizationsSuccess,
  fetchOrganizationsFailed
] = createAPIActions('Fetch Organization');

export const [
  fetchOrganizationScheme,
  fetchOrganizationSchemeSuccess,
  fetchOrganizationSchemeFailed,
] = createAPIActions('Fetch Organization Scheme');

const onCreate = state =>
  ({ ...state, loading: true, error: null });

const onCreateSuccess = (state, organization) =>
  ({ ...state, loading: false, data: [...state.data, organization] });

const onCreateFailed = (state, payload) =>
  ({ ...state, loading: false, error: payload });

const onFetchOrgScheme = state =>
  ({ ...state, loading: true, error: null });

const onFetchOrgSchemeSuccess = (state, { path, scheme }) =>
  ({
    ...state,
    loading: false,
    data: state.data.map((org) => {
      if (org.path === path) {
        return { ...org, scheme };
      }
      return org;
    })
  });

const onFetchOrgSchemeFailed = (state, err) =>
  ({ ...state, loading: false, error: err });

const onFetchOrganizations = state =>
  ({ ...state, loading: true, error: null });

const onFetchOrganizationsSuccess = (state, organizations) =>
  ({ ...state, loading: false, data: uniqBy([...state.data, ...organizations], 'id') });

const onFetchOrganizationsFailed = (state, error) =>
  ({ ...state, loading: false, error });

export default createReducer({
  [createOrganization]: onCreate,
  [createOrganizationSuccess]: onCreateSuccess,
  [createOrganizationFailed]: onCreateFailed,

  [fetchOrganizations]: onFetchOrganizations,
  [fetchOrganizationsSuccess]: onFetchOrganizationsSuccess,
  [fetchOrganizationsFailed]: onFetchOrganizationsFailed,

  [fetchOrganizationScheme]: onFetchOrgScheme,
  [fetchOrganizationSchemeFailed]: onFetchOrgSchemeFailed,
  [fetchOrganizationSchemeSuccess]: onFetchOrgSchemeSuccess
}, initialState);

function* processCreateOrganization({ payload }) {
  const organization = get(payload, 'organization');
  const resolve = get(payload, 'resolve');
  const reject = get(payload, 'reject');

  try {
    const response = yield call(Api.organizations.create, organization);
    yield resolve(response.data);
    yield put(createOrganizationSuccess(response.data));
  } catch (e) {
    yield reject(e);
    yield put(createOrganizationFailed(e.response.data));
  }
}

function* processFetchOrgScheme({ payload }) {
  try {
    const { path } = payload;
    const response = yield call(Api.organizations.getScheme, path);
    const scheme = get(response, 'data');

    yield put(fetchOrganizationSchemeSuccess({
      path,
      scheme
    }));
  } catch (e) {
    yield put(fetchOrganizationSchemeFailed(get(e, 'data')));
  }
}

function* processFetchOrganization() {
  try {
    const response = yield call(Api.organizations.get);
    yield put(fetchOrganizationsSuccess(response.data));
  } catch (e) {
    yield put(fetchOrganizationsFailed(e.data));
  }
}

function* fetchOrgSchemeWatcher() {
  yield takeEvery(fetchOrganizationScheme, processFetchOrgScheme);
}

function* createOrgWatcher() {
  yield takeEvery(createOrganization, processCreateOrganization);
}

function* fetchOrganizationWatcher() {
  yield takeEvery(fetchOrganizations, processFetchOrganization);
}

export function* organizationsSagas(): ForkEffect {
  yield all([
    fork(createOrgWatcher),
    fork(fetchOrgSchemeWatcher),
    fork(fetchOrganizationWatcher)
  ]);
}
