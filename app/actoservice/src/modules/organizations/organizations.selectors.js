/**
 * @flow
 */
import { createSelector } from 'reselect';
import get from 'lodash/get';

export const selectOrganizations = createSelector(
  state => state.organizations,
  organizations => ({
    organizationsError: organizations.error,
    organizationsLoading: organizations.loading,
    organizations: organizations.data
  })
);

export const selectOrganizationById = (id: number) =>
  createSelector(
    _state => _state.organizations.data,
    organizations => organizations.find(org => org.id === id)
  );


export const selectOrganizationScheme = (id: number) =>
  createSelector(
    state => state.organizations.data.find(org => org.id === id),
    organization => get(organization, 'scheme')
  );
