// @flow

import {
  createStore,
  combineReducers,
  applyMiddleware
} from 'redux';

import createSagaMiddleware from 'redux-saga';
import { reducer as formReducer } from 'redux-form';

import { fork, all } from 'redux-saga/effects';

import {
  userReducer,
  userSagas
} from './user';
import {
  organizationsReducer,
  organizationsSagas
} from './organizations';
import {
  themesReducer,
  themesSagas,
} from './themes';
import {
  tokenReducer,
  updateToken
} from './token';
import {
  templatesReducer,
  templatesSagas
} from './templates';


function mergeReducers() {
  return combineReducers({
    user: userReducer,
    form: formReducer,
    themes: themesReducer,
    organizations: organizationsReducer,
    token: tokenReducer,
    templates: templatesReducer
  });
}
function* mergeSagas() {
  yield all([
    fork(userSagas),
    fork(organizationsSagas),
    fork(themesSagas),
    fork(templatesSagas)
  ]);
}

export function create() {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [sagaMiddleware];

  const store = createStore(
    mergeReducers(),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(...middlewares)
  );

  sagaMiddleware.run(mergeSagas);
  updateToken.assignTo(store);

  return store;
}

const store = create();

export default store;
