export {
  default as templatesReducer,
  templatesSagas,
  fetchTemplate,
  updateTemplate
} from './templates.module';

export { selectTemplate } from './templates.selectors';
