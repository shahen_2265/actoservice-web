import {
  takeEvery,
  call,
  put,
  all,
  fork
} from 'redux-saga/effects';
import { createReducer } from 'redux-act';
import uniqBy from 'lodash/uniqBy';
import { createAPIActions } from 'utils/reduxUtils';
import Api from 'api';

const initialState = {
  data: [],
  loading: false,
  error: null
};
export const [
  fetchTemplate,
  fetchTemplateSuccess,
  fetchTemplateFailed
] = createAPIActions('Fetch Template');

export const [
  updateTemplate,
  updateTemplateSuccess,
  updateTemplateFailed,
] = createAPIActions('Update Template');

const onFetchTemplate = state =>
  ({ ...state, loading: true, error: null });

const onFetchTemplateSuccess = (state, template) =>
  ({ ...state, loading: false, data: uniqBy([...state.data, template], 'path') });

const onFetchTemplateFailed = (state, error) =>
  ({ ...state, loading: false, error });

const onUpdateTemplate = state =>
  ({ ...state, loading: true, error: null });

const onUpdateTemplateFailed = (state, error) =>
  ({ ...state, loading: false, error });

const onUpdateTemplateSuccess = (state, newTemplate) =>
  ({ ...state, loading: false, newTemplate });

export default createReducer({
  [fetchTemplate]: onFetchTemplate,
  [fetchTemplateSuccess]: onFetchTemplateSuccess,
  [fetchTemplateFailed]: onFetchTemplateFailed,

  [updateTemplate]: onUpdateTemplate,
  [updateTemplateSuccess]: onUpdateTemplateSuccess,
  [updateTemplateFailed]: onUpdateTemplateFailed
}, initialState);

function* processFetchTemplate({ payload }) {
  const { path } = payload;
  try {
    const response = yield call(Api.templates.fetchTemplate, path);
    yield put(fetchTemplateSuccess({
      path,
      scheme: response.data
    }));
  } catch (e) {
    yield put(fetchTemplateFailed(e));
  }
}

function* processUpdateTemplate({ payload }) {
  const { data, resolve, reject } = payload;
  try {
    const response = yield call(Api.templates.update, data);
    yield put(updateTemplateSuccess(response.data));
    yield call(resolve);
  } catch (e) {
    yield put(updateTemplateFailed(e));
    yield call(reject);
  }
}


function* watchFetchTemplate() {
  yield takeEvery(fetchTemplate, processFetchTemplate);
}

function* watchUpdateTemplate() {
  yield takeEvery(updateTemplate, processUpdateTemplate);
}

export function* templatesSagas() {
  yield all([
    fork(watchFetchTemplate),
    fork(watchUpdateTemplate)
  ]);
}
