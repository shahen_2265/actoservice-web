/**
 * @flow
 */
import { createSelector } from 'reselect';
import get from 'lodash/get';

export const selectTemplate = (path: string) =>
  createSelector(
    state => state.templates,
    templates => ({
      loading: templates.loading,
      error: templates.error,
      scheme: get(templates.data.find(tmp => tmp.path === path), 'scheme')
    })
  );
