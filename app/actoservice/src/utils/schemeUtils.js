import get from 'lodash/get';
import uniqBy from 'lodash/uniqBy';
import set from 'lodash/set';

export function keyify(obj, prefix = '') {
  return Object.keys(obj).reduce((res, el) => {
    if (Array.isArray(obj[el])) {
      return [
        ...res,
        prefix + el
      ];
    }
    if (obj[el] !== null && typeof obj[el] === 'object') {
      return [
        ...res,
        ...keyify(obj[el], `${prefix}${el}.`)
      ];
    }
    return [...res, prefix + el];
  }, []);
}

export const parseKeyItems = (keyPath, joinStr = '') =>
  `${keyPath.substr(0, keyPath.lastIndexOf('.'))}${joinStr}`;

export const getDescriptionPath = keyPath =>
  parseKeyItems(keyPath, '.__description__');

export const getTitlePath = keyPath =>
  parseKeyItems(keyPath, '.__title__');

export function parseScheme(scheme, template) {
  const parsedResult = [];
  console.log(scheme, template);
  const allKeys = keyify(scheme);

  allKeys.forEach((keyPath) => {
    const path = parseKeyItems(keyPath);
    const key = getTitlePath(keyPath);
    const description = getDescriptionPath(keyPath);

    parsedResult.push({
      path,
      key: get(scheme, key),
      value: get(template, path),
      description: get(scheme, description),
    });
  });

  return uniqBy(parsedResult, 'path');
}


export const mixTemplateScheme = (templateJSON, scheme) =>
  keyify(templateJSON)
    .reduce((all, currPath: string) => {
      set(all, `${currPath}.value`, get(templateJSON, `${currPath}`));
      set(all, `${currPath}.type`, get(scheme, `${currPath}.type`));
      set(all, `${currPath}.title`, get(scheme, `${currPath}.__title__`));
      return all;
    }, {});
