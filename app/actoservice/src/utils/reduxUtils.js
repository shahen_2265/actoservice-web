import { createAction } from 'redux-act';

/**
 * @param {String} actionDescription
 * @returns {Array} - Notify | Success | Failed Actions
 */
export function createAPIActions(actionDescription) {
  return [
    createAction(actionDescription),
    createAction(`${actionDescription} - Success`),
    createAction(`${actionDescription} - Failed`),
  ];
}
