/**
 * @flow
*/

import isNil from 'lodash/isNil';

type Payload = {
  [key: string]: *
};


export function serializeParam(data: Payload): string {
  return Object.keys(data)
    .filter(key => !isNil(data[key]))
    .reduce((query, curr, index) => {
      return `${query}${index === 0 ? '' : '&'}${curr}=${data[curr]}`;
    }, '?');
}
