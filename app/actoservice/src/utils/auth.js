import isNil from 'lodash/isNil';
import { AUTH_LOCAL_STORAGE_KEY } from 'constants';

export const isAuthenticated = () =>
  !isNil(window.localStorage.getItem(AUTH_LOCAL_STORAGE_KEY));
