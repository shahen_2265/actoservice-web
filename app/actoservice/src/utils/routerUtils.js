import Loadable from 'react-loadable';
import ComponentLoader from 'components/ComponentLoader';

export function makeComponentLoadable(loadComponent) {
  return Loadable({
    loader: loadComponent,
    loading: ComponentLoader
  });
}
