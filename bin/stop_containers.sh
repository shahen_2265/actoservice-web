echo "|> Stopping Containers"

docker ps -a | awk '{ print $1 }' | grep -v "CONTAINER" | xargs docker stop

echo "|> Removing Containers"
docker ps -a | awk '{ print $1 }' | grep -v "CONTAINER" | xargs docker rm