#!/usr/bin/env bash

echo "Cleaning dist folder"

rm -rf ./dist

echo "Building Server"

NODE_ENV=production ./node_modules/.bin/babel ./src -d dist --ignore '__tests__'

echo "Server built successfully"
