#!/usr/bin/env bash

prepared_command='sudo docker ps --filter status="exited" | awk "{print $1}" | grep -v CONTAINER | xargs sudo docker rm && sudo docker images | xargs sudo docker rmi'

ssh -i "~/Downloads/Actoservice-MacbookPro.pem" ubuntu@ec2-18-205-189-211.compute-1.amazonaws.com 'cd /var/app/actoservice && sudo git pull && sudo ./bin/stop_containers.sh && sudo ./bin/build_prod.sh'