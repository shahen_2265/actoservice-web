const generator = require('./webpack.config.generator');
const { BUILD_ENV } = require('./webpack.config.generator');

module.exports = generator(
  BUILD_ENV.ACTOSERVICE,
  process.env.NODE_ENV === 'production'
);
